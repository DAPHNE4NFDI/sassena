"""
Arnab Majumdar

Date: 04.04.2023

This is a tool to check how sassena has read a pdb file

Usage:

python3 pdb_analyzer.py --file <file_name>

help:

python3 pdb_analyzer.py --help

Required packages:

numpy, xml, os, argparse, regex

"""

import numpy as np
import xml.etree.cElementTree as ET
import os
import re
import argparse
import h5py


def pdb_reader(file_name):
    pdb_file= open(file_name,'r')
    atom_names=[]
    for line in pdb_file:
        words=line.split()
        if words[0]=='ATOM':
            atom_names.append(words[2])
    pdb_file.close()
    atom_types=[*set(atom_names)]
    return atom_types, atom_names

def database_reader(db_dir):
    name_file=os.path.join(db_dir,'database/definitions/names.xml')
    tree = ET.parse(name_file)
    root = tree.getroot()
    names=[]
    for child in root:
        for grchild in child:
            name=[]
            for grgrchild in grchild:
                name.append(grgrchild.text)
            names.append(name)
    sf_file=os.path.join(db_dir,'database/definitions/scatterfactors-neutron-coherent.xml')
    tree = ET.parse(sf_file)
    root = tree.getroot()
    sfs=[]
    for child in root:
        sf=[]
        for grchild in child:
            sf.append(grchild.text)
        sfs.append(sf)
    return names, sfs

def final_database(names,sfs,res_idx):
    data=[]
    for name in names:
        el_name=name[0]
        for sf in sfs:
            if sf[0]==el_name:
                el_sf=sf[res_idx]
                datum=[el_name, name[1], el_sf]
                break
        data.append(datum)
    return data

def print_results(atom_names,atom_types,data):
    self_term=0
    for atom_type in atom_types:
        type_count=atom_names.count(atom_type)
        for datum in data:
            
            if re.search(datum[1],atom_type):
                print(atom_type, ':', datum[0], ':', type_count, ':', datum[2])
                self_term+=float(type_count)*float(datum[2])**2

def txtwriter(txtfile,atom_names,atom_types,data,pdb_file):
    file=open(txtfile,'w')
    header1='This file gives a summary about the pdb file '+pdb_file+' and how sassena read it\n\n\n'
    header2="{0:<20} {1:<20} {2:<20}".format('pdb_symbol', 'sassena', 'count')+'\n\n'
    file.write(header1+header2)
    for atom_type in atom_types:
        type_count=atom_names.count(atom_type)
        for datum in data:
            if re.search(datum[1],atom_type):
                cur_str="{0:<20} {1:<20} {2:<20}".format(atom_type, datum[0], str(type_count))+'\n'
                file.write(cur_str)
    file.close()
    
                
def parse_args():
    desc = "Analysis tool for sassena read of pdb"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--file', type=str,
                        help='Provide the full path with name of pdb file', required=True)
    parser.add_argument('--db_dir', default='.', type=str, help='Provide the name and full path of database directory.')
    parser.add_argument('--update', default=False, type=bool, help='update the database with original.')
    parser.add_argument('--txt_file', default='data.txt', type=str, help='name of the output txt file')
    return parser.parse_args()

def main():
    args = parse_args()
    pdb_file_name=args.file
    db_dir=args.db_dir
    txt_file=args.txt_file
    if (args.update or not os.path.isdir(os.path.join(args.db_dir,'database'))):
        os.system('cp -r ../tests/waterdyn4/database .')
        print('cp -r ../tests/waterdyn4/database .')
    atom_types, atom_names = pdb_reader(pdb_file_name)
    names, sfs=database_reader(db_dir)
    data = final_database(names,sfs,2)
    txtwriter(txt_file,atom_names,atom_types,data,pdb_file_name)
    
if __name__ == '__main__':
    main()
