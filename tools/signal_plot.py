import h5py
import argparse, os
import numpy as np

import matplotlib.pyplot as plt

def plot_2d(y,x,style,file_num,param):
    label_name=str(file_num)+'_'+param
    idx_sort=np.argsort(y)
    y=np.array(y)[idx_sort]
    x=np.array(x)[idx_sort]
    plt.loglog(y,x, style,label=label_name)

def vector_mag(vector):
    if (len(vector.shape) > 1):
        dim = vector.shape[1]
    else:
        dim=1
    vector_mag=np.zeros(vector.shape[0])
    for i in range(dim):
        vector_mag+=vector[:,i]**2
    return np.sqrt(vector_mag)

def signal_reader(filename,parameter):
    file_read=h5py.File(filename, 'r')
    q_vec=file_read['qvectors']
    param=file_read[parameter]
    return vector_mag(q_vec), vector_mag(param)
    
    
def parse_args():
    desc = "plotting tool from out put of sassena"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--file', type=str, default='signal.h5',
                        help='prove the path and name of file. [multiple file: "file1 file2"]')#, required=True)
    parser.add_argument('--parameter', type=str, default='fq',
                        help='name the parameter that to plotted [e.g. fq, fq0, fqt or fq2]')
    return parser.parse_args()

def main():
    # parse arguments
    args = parse_args()
    if args is None:
        exit()

    # read filename and parameter name to be plotted
    file_list = args.file
    files = file_list.split()
    parameter=args.parameter

    # plotting figure 
    plt.figure()
    file_num=0
    for file in files:
        style='-'
        x, y = signal_reader(file, parameter)
        plot_2d(x,y,style,file_num,parameter)
        file_num+=1
    plt.legend()
    #plt.yscale
    plt.show()
if __name__ == '__main__':
    main()
