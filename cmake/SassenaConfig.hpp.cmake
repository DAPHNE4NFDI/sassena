#pragma once

#define Sassena_VERSION_MAJOR    @Sassena_VERSION_MAJOR@
#define Sassena_VERSION_MINOR    @Sassena_VERSION_MINOR@
#define Sassena_VERSION_PATCH    @Sassena_VERSION_PATCH@
#define Sassena_VERSIONSTRING "@Sassena_VERSION_MAJOR@.@Sassena_VERSION_MINOR@.@Sassena_VERSION_PATCH@"

#cmakedefine01 SASS_USE_SINGLE_PRECISION
#cmakedefine01 SASS_USE_CUDA
#cmakedefine01 SASS_USE_SYCL


