add_subdirectory(xdrfile-1.1.1)

# Taskflow Config


if (SASS_USE_CUDA)
option(TF_BUILD_CUDA "Enables builds of cuda code" ON)
endif()
if (SASS_USE_SYCL)
option(TF_BUILD_SYCL "Enables builds of sycl code" OFF)
endif()

option(TF_BUILD_BENCHMARKS "Enables builds of benchmarks" OFF)
option(TF_BUILD_TESTS "Enables builds of tests" OFF)
option(TF_BUILD_EXAMPLES "Enables builds of examples" OFF)

add_subdirectory(taskflow)
