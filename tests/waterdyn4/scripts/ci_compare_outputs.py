#!/usr/bin/env python3

import h5py
import argparse, os
import sys
import numpy as np

def read_h5_file(file_name, param):
    f = h5py.File(file_name, 'r')
    qvs = np.array(f['qvectors'])
    idxs = np.argsort(qvs, axis=0)
    sorted_params = np.array(f[param])[idxs]
    return sorted_params


def compare_files(files, param):
    if len(files) != 2:
        print("Error: Please provide two files to compare")
        exit()

    file1 = read_h5_file(files[0], param)
    file2 = read_h5_file(files[1], param)

    return np.allclose(file1, file2)


def main():
    # parse arguments
    parser = argparse.ArgumentParser(description='Tool to compare the h5 outputs of Sassena')
    parser.add_argument('--file',
                        type=str,
                        default='signal.h5',
                        help='Provide two h5 files to compare: "file1.h5 file2.h5"',
                        nargs=2)
    parser.add_argument('--parameter',
                        type=str,
                        default='fq',
                        help='The parameter to be compared [e.g. fq, fq0, fqt or fq2]')

    args = parser.parse_args()
    if args is None:
        exit()

    if compare_files(args.file, args.parameter) == True:
        print("The HDF5 files are the same")
        return sys.exit(0)
    else:
        print("The HDF5 files are different")
        return sys.exit(1)

if __name__ == "__main__":
    main()
