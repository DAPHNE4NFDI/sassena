/** \file
The content of this file is included by any other file within the project. Use
it to apply application wide modifications.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#include "common.hpp"

double sinc( double x){
      if (x==0)
        return 1;
      else
        return sin(x)/x;
    }


// end of file
