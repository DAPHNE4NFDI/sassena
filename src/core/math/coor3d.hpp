/** \file
This file contains definitions for coordinate vector types for different
coordinate systems.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#ifndef MATH__COOR3D_HPP_
#define MATH__COOR3D_HPP_

// common header
#include "common.hpp"

// standard header
#include <array>
#include <iostream>

// special library headers
#include <boost/serialization/access.hpp>
#include <spdlog/formatter.h>

class CartesianCoor3D;
class CylinderCoor3D;
class SphericalCoor3D;

using cartrect = std::pair<CartesianCoor3D, CartesianCoor3D>;

// helper funcions:
inline float sign(float a, float b)
{
    return (b < 0.0) ? -a : a;
}

/**
Type class which represents coordinates in cartesian space. Allows
transformation into other coordinate representations and implements some basic
linear algebra.
*/
class CartesianCoor3D
{
protected:
    // make this class serializable to
    // allow sample to be transmitted via MPI
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
        ar &x;
        ar &y;
        ar &z;
    }
    ///////////////////
public:
    // TODO: do we need doubles here?
    coor2_t x, y, z;

    CartesianCoor3D()
        : x(0.0)
        , y(0.0)
        , z(0.0)
    {
    }
    CartesianCoor3D(coor2_t v1, coor2_t v2, coor2_t v3)
        : x(v1)
        , y(v2)
        , z(v3)
    {
    }
    // conversion constructors:
    // NOTE: don't make explicit as implicit conversions used in code-base
    CartesianCoor3D(CylinderCoor3D cc);
    CartesianCoor3D(SphericalCoor3D cc);

    CartesianCoor3D(const CartesianCoor3D &that) = default;
    CartesianCoor3D(CartesianCoor3D &&that) = default;
    CartesianCoor3D &operator=(CartesianCoor3D &&that) = default;

    coor2_t length() const;

    friend std::ostream &operator<<(std::ostream &os, const CartesianCoor3D &cc);

    CartesianCoor3D &operator=(const CartesianCoor3D &that);
    CartesianCoor3D operator-(const CartesianCoor3D &that);
    CartesianCoor3D operator+(const CartesianCoor3D &that);
    CartesianCoor3D cross_product(const CartesianCoor3D &that);
    coor2_t operator*(const CartesianCoor3D &that);

    // for use in maps only!
    bool operator<(const CartesianCoor3D &that) const;

    ~CartesianCoor3D() = default;
};

CartesianCoor3D operator*(coor2_t lambda, const CartesianCoor3D &that);
CartesianCoor3D operator*(const CartesianCoor3D &that, coor2_t lambda);
CartesianCoor3D operator/(const CartesianCoor3D &that, coor2_t lambda);

template<>
struct fmt::formatter<CartesianCoor3D>
{
    constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin())
    {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const CartesianCoor3D &input, FormatContext &ctx) -> decltype(ctx.out())
    {
        return format_to(ctx.out(), "(x={}, y={}, z={})", input.x, input.y, input.z);
    }
};

/**
Type class which represents coordinates in cylinder space. Allows transformation
into other coordinate representations and implements some basic linear algebra.

Cylinder coords have a range:
- r >= 0
- 0 <= phi < 2 M_PI
*/
class CylinderCoor3D
{
public:
    // TODO: do we need doubles here?
    coor2_t r, phi, z;

    CylinderCoor3D()
        : r(0)
        , phi(0)
        , z(0)
    {
    }
    CylinderCoor3D(coor2_t v1, coor2_t v2, coor2_t v3);
    // conversion constructors:
    explicit CylinderCoor3D(CartesianCoor3D cc);
    explicit CylinderCoor3D(SphericalCoor3D cc);

    friend std::ostream &operator<<(std::ostream &os, const CylinderCoor3D &cc);

    CylinderCoor3D &operator=(const CylinderCoor3D &that);
    CylinderCoor3D operator-(const CylinderCoor3D &that);

    ~CylinderCoor3D() = default;
};

/**
Type class which represents coordinates in spherical space. Allows
transformation into other coordinate representations and implements some basic
linear algebra.

Spherical coords have a range:
- r >= 0
- 0 <= phi < 2 M_PI
- 0 <= theta < M_PI
*/
class SphericalCoor3D
{
public:
    // TODO: do we need doubles here?
    coor2_t r = 0;
    coor2_t phi = 0;
    coor2_t theta = 0;

    SphericalCoor3D() = default;
    SphericalCoor3D(coor2_t v1, coor2_t v2, coor2_t v3)
        : r(v1)
        , phi(v2)
        , theta(v3)
    {
    }
    // conversion constructors:
    SphericalCoor3D(CartesianCoor3D cc);
    SphericalCoor3D(CylinderCoor3D cc);

    friend std::ostream &operator<<(std::ostream &os, const SphericalCoor3D &cc);

    SphericalCoor3D &operator=(const SphericalCoor3D &that);
    SphericalCoor3D operator-(const SphericalCoor3D &that);

    ~SphericalCoor3D() = default;
};

CartesianCoor3D rotate(CartesianCoor3D, std::string axis, coor2_t rad);

/**
Type class which represents a vector-space basis (3 orthonormal vectors) for cartesian
coordinates. Can be constructed out of thin air or from partial vectors.
*/
class CartesianVectorBase
{
    std::array<CartesianCoor3D, 3> base_;

public:
    CartesianVectorBase() = default;
    explicit CartesianVectorBase(CartesianCoor3D axis);

    auto get_base() const
    {
        return base_;
    }

    CartesianCoor3D &operator[](size_t index);
    CartesianCoor3D project(CartesianCoor3D vec);
};

// TODO: Is this needed when std::complex exsits?
class ImagNum
{
protected:
    // make this class serializable to
    // allow sample to be transmitted via MPI
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
        ar &re;
        ar &im;
    }
    ///////////////////
public:
    coor2_t re = 0;
    coor2_t im = 0;

    ImagNum() = default;
    ImagNum(coor2_t r, coor2_t i)
        : re(r)
        , im(i)
    {
    }
    ~ImagNum() = default;

    inline coor2_t length() const;

    friend std::ostream &operator<<(std::ostream &os, const ImagNum &in);

    ImagNum &operator=(const ImagNum &that);
    ImagNum operator-(const ImagNum &that);
    ImagNum operator+(const ImagNum &that);
    ImagNum operator*(const ImagNum &that);
};

ImagNum operator*(coor2_t lambda, const ImagNum &that);
ImagNum operator*(const ImagNum &that, coor2_t lambda);
ImagNum operator/(const ImagNum &that, coor2_t lambda);
ImagNum operator/(coor2_t lambda, const ImagNum &that);


#endif

// end of file
