/** \file
This file contains a class which defines an atom.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#ifndef SAMPLE__ATOM_HPP_
#define SAMPLE__ATOM_HPP_

// common header
#include "common.hpp"

// standard header
#include <string>

// special library headers
#include <boost/serialization/access.hpp>
#include <boost/serialization/string.hpp>

// other headers

/**
Type class which represents an atom.
*/
class Atom
{
    // make this class serializable to
    // allow sample to be transmitted via MPI
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
        ar &ID;
        ar &name;

        ar &index;
        ar &mass;
    }
    ///////////////////
public:
    size_t ID;
    std::string name;

    size_t index;
    double mass;
};

#endif

// end of file
