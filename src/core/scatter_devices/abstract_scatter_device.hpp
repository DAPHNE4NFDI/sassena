/** \file
This file contains the interface definition for all scattering devices and
implements an abstract scattering device from which all other devices are
derived.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/
#pragma once

#include <memory>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/mpi.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/thread.hpp>
#include <fftw3.h>

#include <taskflow/taskflow.hpp>

#include "common.hpp"

#include "decomposition/assignment.hpp"
#include "i_scatter_device.hpp"
#include "math/coor3d.hpp"
#include "report/timer.hpp"
#include "scatter_factors.hpp"
#include "services.hpp"


/**
Efficent Thread-safe version of a queue
*/
template<typename Data>
class concurrent_queue
{
private:
    std::queue<Data> the_queue;
    mutable boost::mutex the_mutex;
    boost::condition_variable the_condition_variable;

public:
    void push(Data const &data)
    {
        boost::mutex::scoped_lock lock(the_mutex);
        the_queue.push(data);
        lock.unlock();

        // notify everyone who tries to push
        the_condition_variable.notify_all();
    }

    bool empty() const
    {
        boost::mutex::scoped_lock lock(the_mutex);
        return the_queue.empty();
    }

    size_t size() const
    {
        return the_queue.size();
    }

    bool try_pop(Data &popped_value)
    {
        boost::mutex::scoped_lock lock(the_mutex);
        if (the_queue.empty()) {
            return false;
        }

        popped_value = the_queue.front();
        the_queue.pop();
        // notify everyone who tries to push
        return true;
    }

    void wait_and_pop(Data &popped_value)
    {
        boost::mutex::scoped_lock lock(the_mutex);
        while (the_queue.empty()) {
            the_condition_variable.wait(lock);
        }

        popped_value = the_queue.front();
        the_queue.pop();
    }

    void wait_for_empty()
    {
        while (!the_queue.empty())
            boost::this_thread::sleep(boost::posix_time::milliseconds(25));
    }
};


/**
Abstract Scattering Device from which all others are derived. It implements
common functionality, e.g. basic control flows.
*/
class AbstractScatterDevice : public IScatterDevice
{
protected:
    tf::Taskflow flow_;

    coor_t *p_coordinates;

    boost::mpi::communicator allcomm_;
    boost::mpi::communicator partitioncomm_;
    Sample &sample_;

    std::vector<CartesianCoor3D> vectors_;
    size_t current_vector_;

    std::shared_ptr<MonitorClient> p_monitor_;
    std::shared_ptr<HDF5WriterClient> p_hdf5writer_;

    // number of nodes, number of frames/time-steps , number of atoms
    size_t NN, NF, NA;

    fftw_complex *atfinal_ = nullptr;
    // temporary storage for reduce & write phase
    std::complex<double> afinal_;
    std::complex<double> a2final_;

    ScatterFactors scatterfactors;

    virtual void stage_data() = 0;
    virtual void compute() = 0;

    /// @brief Compute next Q-vector
    void next();
    void write();

    void runner() override;

    virtual tf::Taskflow create_flow()
    {
        return {};
    }

    virtual void print_pre_stage_info()
    {
    }
    virtual void print_post_stage_info()
    {
    }
    virtual void print_pre_runner_info()
    {
    }
    virtual void print_post_runner_info()
    {
    }

    virtual bool ram_check();
    void start_workers();
    void stop_workers();
    virtual void worker() = 0;
    std::queue<boost::thread *> worker_threads;
    boost::barrier *workerbarrier;

    /// @brief If there are still Q-vectors left to compute
    /// @return 0 if there are still Q-vectors left to compute, else 1
    bool status() override;
    double progress() override;

    std::map<boost::thread::id, Timer> timer_;

public:
    std::map<boost::thread::id, Timer> &getTimer() override;

    AbstractScatterDevice(boost::mpi::communicator allcomm, boost::mpi::communicator partitioncomm,
                          Sample &sample, std::vector<CartesianCoor3D> vectors, size_t NAF,
                          boost::asio::ip::tcp::endpoint fileservice_endpoint,
                          boost::asio::ip::tcp::endpoint monitorservice_endpoint);
    ~AbstractScatterDevice();

    void run() override;
};


// end of file
