/** \file
This file contains a class which manages the generation of the proper scattering
factors which may depend on atom and q vector lengths.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#ifndef SCATTER_DEVICES__SCATTER_FACTORS_HPP_
#define SCATTER_DEVICES__SCATTER_FACTORS_HPP_

// common header
#include "common.hpp"

// standard header
#include <string>
#include <vector>

// special library headers

// other headers
#include "sample.hpp"

// forward declaration...

class ReciprocalBox
{
    ImagNum _sld; // scattering length density in reciprocal space
    double _xlength;
    double _ylength;
    double _zlength;
    CartesianCoor3D _midpoint;

public:
    ReciprocalBox();

    // use these to initialize the reciprocal box:
    void set_all();
    ImagNum get_sld();
    double get_xlength();
    double get_ylength();
    double get_zlength();
    CartesianCoor3D get_midpoint();
};

/**
Efficient management class for scattering factors.
*/
class ScatterFactors
{
    Sample *p_sample;
    IAtomselection *p_selection;

    bool m_background;

    // TODO: do we need doubles here if using single precision?
    std::vector<double> factors;
    std::vector<double> m_kappas;

    ReciprocalBox _ReciBox;

public:
    ScatterFactors();

    // use these to initialize the scatterfactors set:
    void set_selection(IAtomselection *selection);
    void set_sample(Sample &sample);

    IAtomselection *get_selection();

    double get(size_t atomselectionindex) const;
    const std::vector<double> &get_all() const;
    ReciprocalBox get_recibox();
    void update(CartesianCoor3D q);
    void update_kappas();
    void update_reciprocal_box();
    double compute_background(CartesianCoor3D q, IAtomselection *selection = nullptr);

    void set_background(bool status);
};


#endif

// end of file
