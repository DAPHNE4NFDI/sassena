/** \file
This file contains a class which implements the scattering calculation for self
scattering.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#pragma once


// special library headers
#include <boost/asio.hpp>
#include <boost/mpi.hpp>
#include <fftw3.h>

// common header
#include "common.hpp"

// other headers
#include "math/coor3d.hpp"
#include "report/timer.hpp"
#include "sample.hpp"

#include "abstract_vectors_scatter_device.hpp"


/**
Implements self type scattering using vectors for orientational averaging (incoherent scattering)
* Computes scattering amplitudes a_n(q,t)
* Alignment by atoms
*/
class SelfVectorsScatterDevice : public AbstractVectorsScatterDevice
{
public:
    SelfVectorsScatterDevice(boost::mpi::communicator allcomm,
                             boost::mpi::communicator partitioncomm, Sample &sample,
                             std::vector<CartesianCoor3D> vectors, size_t NAF,
                             boost::asio::ip::tcp::endpoint fileservice_endpoint,
                             boost::asio::ip::tcp::endpoint monitorservice_endpoint);
    SelfVectorsScatterDevice(const SelfVectorsScatterDevice &) = delete;
    SelfVectorsScatterDevice operator=(const SelfVectorsScatterDevice &) = delete;

protected:
    // array of output
    fftw_complex *at_;
    // first = qvec index, second = atomindex
    concurrent_queue<std::pair<size_t, size_t>> atscatter_;
    // Allocated by the data stager (must free ourselves)
    coor_t *p_coordinates;

    size_t current_atomindex_ = 0;

    size_t coord_size_ = 0;

    // calculate amplitudes for a given q and atom for all frames
    fftw_complex *scatter(size_t qindex, size_t aindex);

    ModAssignment assignment_;

    double progress() override;

    void stage_data() override;

    void worker() override;
    void compute() override;

    // push count q indexes of (atomindex, q index) to the atscatter queue
    void scatterblock(size_t atomindex, size_t index, size_t count);

    void store(fftw_complex *at);
    void dsp(fftw_complex *at);

    bool ram_check() override;

    ~SelfVectorsScatterDevice();
    fftw_plan fftw_planF_;
    fftw_plan fftw_planB_;
};
