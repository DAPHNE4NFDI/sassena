/** \file
This file contains all the kernels used by the CUDA self scattering device.

\author Daniel Vonk <dan@danvonk.com>
\version 1.3.0
\copyright GNU General Public License
*/

#pragma once

#include "math/coor3d.hpp"
#include "math/smath.hpp"

#include "common.hpp"


namespace sass {
namespace cuda {

__global__ void cuda_scatter(coor_t *p_coords, CartesianCoor3D *subvector_index, size_t N,
                             size_t NF, fftw_complex *at, double *scatterfactors, size_t index,
                             size_t aindex);
__global__ void square_elements(fftw_complex *at, size_t N, size_t NF);
__global__ void store(fftw_complex *afinal, fftw_complex *a2final, fftw_complex *at, size_t N,
                      size_t NF);
__global__ void store_atfinal(fftw_complex *atfinal, fftw_complex *at, size_t N, size_t NF);
__global__ void autocorrelate_pow_spect(fftw_complex *at, size_t N, size_t NF);
__global__ void autocorrelate_normalize(fftw_complex *at, size_t N, size_t NF);
}
}
