/** \file
This file contains a class which implements the scattering calculation for self
scattering.

\author Daniel Vonk <dan@danvonk.com>
\version 1.3.0
\copyright GNU General Public License
*/

#include <cuda_runtime.h>
#include <cufft.h>
#include <taskflow/cuda/cudaflow.hpp>

#include <complex>
#include <thread>

// special library headers
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/lexical_cast.hpp>

#include "self_vectors_cuda_scatter_device.hpp"

#include "control.hpp"
#include "log.hpp"
#include "math/coor3d.hpp"
#include "math/smath.hpp"
#include "sample.hpp"
#include "stager/data_stager.hpp"

#include "self_vectors_scatter_ker.cuh"

SelfVectorsCUDAScatterDevice::SelfVectorsCUDAScatterDevice(
    boost::mpi::communicator allcomm, boost::mpi::communicator partitioncomm, Sample &sample,
    std::vector<CartesianCoor3D> vectors, size_t NAF,
    boost::asio::ip::tcp::endpoint fileservice_endpoint,
    boost::asio::ip::tcp::endpoint monitorservice_endpoint)
    : AbstractVectorsXPUScatterDevice(std::move(allcomm), std::move(partitioncomm), sample,
                                      std::move(vectors), NAF, std::move(fileservice_endpoint),
                                      std::move(monitorservice_endpoint))
    , assignment_(partitioncomm_.size(), partitioncomm_.rank(), NAF)
{
    int n_devs = 0;
    cudaGetDeviceCount(&n_devs);

    if (n_devs > 0) {
        cudaDeviceProp p;
        cudaGetDeviceProperties(&p, 0);

        global_memory_ = p.totalGlobalMem;
        shared_mem_per_block_ = p.sharedMemPerBlock;
        max_threads_per_block_ = p.maxThreadsPerBlock;
        max_threads_per_sm_ = p.maxThreadsPerMultiProcessor;
        num_sm_ = p.multiProcessorCount;
    } else {
        sass::err(
            "Could not enumerate CUDA device properties. All parameter estimates will be wrong.");
    }
}

void SelfVectorsCUDAScatterDevice::stage_data()
{
    Timer &timer = timer_[boost::this_thread::get_id()];
    if (allcomm_.rank() == 0)
        sass::info("Forcing stager.mode=atoms");

    DataStagerByAtom data_stager(sample_, allcomm_, partitioncomm_, timer);
    p_coordinates = data_stager.stage();
    coord_size_ = data_stager.staged_data_size();

    if (allcomm_.rank() == 0) {
        sass::info("Staged {} Mbytes of coordinates onto GPU.", coord_size_ / 1024 / 1204);
        TF_CHECK_CUDA(cudaMalloc(&dcoords, coord_size_), "failed to alloc coords on device");
        cudaMemcpy(dcoords, p_coordinates, coord_size_, cudaMemcpyHostToDevice);
    }
}

SelfVectorsCUDAScatterDevice::~SelfVectorsCUDAScatterDevice()
{
    if (p_coordinates) {
        free(p_coordinates);
        p_coordinates = nullptr;
    }

    cudaFree(dcoords);
}

bool SelfVectorsCUDAScatterDevice::ram_check()
{
    // inherit ram requirements for parent class
    bool state = AbstractVectorsXPUScatterDevice::ram_check();

    if (Params::Inst()->parallel_atoms) {
        max_parallel_atoms_ = *(Params::Inst()->parallel_atoms);
        sass::info("RAM Check: Parallel atoms overriden in config. Will attempt to use {} atoms "
                   "per block for GPU parallel computation.",
                   max_parallel_atoms_);
        return state;
    }


    size_t total_memory, theoretical_max_memory;
    cudaMemGetInfo(&total_memory, &theoretical_max_memory);

    total_memory -= scatterfactors.get_all().size() * sizeof(double);
    total_memory -= coord_size_;
    total_memory -= subvector_index_.size() * sizeof(CartesianCoor3D);

    // datfinal_, dafinal_, da2final_ buffers
    total_memory -= NF * sizeof(fftw_complex);
    total_memory -= 2 * sizeof(fftw_complex);

    // Max bytes required for a signal buffer (per atom)
    size_t bytesize_signal_buffer = 2 * NF * NM * sizeof(fftw_complex);

    // cuFFT requires working space in addition to the signal buffer (per atom)
    if (Params::Inst()->scattering.dsp.type == "autocorrelate") {
        size_t work_size = 0;

        auto res = cufftEstimate1d(2 * NF, CUFFT_Z2Z, NM, &work_size);
        if (res == CUFFT_SUCCESS) {
            bytesize_signal_buffer += work_size;
        } else {
            sass::warn("Could not estimate video RAM required for FFT transforms. Max parallel "
                       "atoms may be overestimated.");
        }
    }

    max_parallel_atoms_ = total_memory / bytesize_signal_buffer;
    max_parallel_atoms_ = max_parallel_atoms_ * 3 / 5; // 60% of available memory as a safety margin

    sass::info("RAM Check: The GPU has enough video memory for {} parallel atom calculations.",
               max_parallel_atoms_);
    sass::info("RAM Check: {} max threads per block. {} max threads per SM. {} SMs available",
               max_threads_per_block_, max_threads_per_sm_, num_sm_);

    return state;
}

void SelfVectorsCUDAScatterDevice::compute()
{
    // called in a loop by abstractstatterdevice (parent class) until we run out of q vectors
    CartesianCoor3D &q = vectors_[current_vector_];

    Timer &timer = timer_[boost::this_thread::get_id()];

    timer.start("sd:c:init");
    init_subvectors(q);
    scatterfactors.update(q); // scatter factors only dependent on length of q,
                              // hence we can do it once before the loop
    current_atomindex_ = 0;

    // temporary storage for single output of a_n
    memset(atfinal_, 0, NF * sizeof(fftw_complex));
    afinal_ = {};
    a2final_ = {};

    cudaMalloc(&datfinal_, NF * sizeof(fftw_complex));
    cudaMalloc(&dafinal_, sizeof(fftw_complex));
    cudaMalloc(&da2final_, sizeof(fftw_complex));
    cudaMemset(datfinal_, 0, NF * sizeof(fftw_complex));
    cudaMemset(dafinal_, 0, sizeof(fftw_complex));
    cudaMemset(da2final_, 0, sizeof(fftw_complex));

    TF_CHECK_CUDA(cudaMalloc(&dscatter_factors, scatterfactors.get_all().size() * sizeof(double)),
                  "failed to alloc scatter factors on device");
    TF_CHECK_CUDA(cudaMalloc(&dsubvector_index, subvector_index_.size() * sizeof(CartesianCoor3D)),
                  "failed to alloc subvector_index on device");

    size_t NNPP = partitioncomm_.size();

    cudaMemcpy(dscatter_factors, scatterfactors.get_all().data(),
               scatterfactors.get_all().size() * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(dsubvector_index, subvector_index_.data(),
               subvector_index_.size() * sizeof(CartesianCoor3D), cudaMemcpyHostToDevice);

    timer.stop("sd:c:init");

    tf::cudaStream stream;
    const size_t ATOM_BLOCK = max_parallel_atoms_;

    std::map<size_t, fftw_complex *> at_ptrs;
    std::map<size_t, cufftHandle> fft_handles;

    // Create at_ptrs and fft_handles in advance and re-use them
    size_t cr_id = 0; // creation ID used to keep track of the at_ptrs and fft_handles
    for (size_t n = 0; n < std::min(ATOM_BLOCK, assignment_.size() - n); ++n) {
        for (size_t i = 0; i < NM; i += NTHREADS) {

            const size_t N = std::min(NTHREADS, NM - i); // number of subvectors in the block

            fftw_complex *d = nullptr;
            TF_CHECK_CUDA(cudaMalloc(&d, 2 * NF * N * sizeof(fftw_complex)),
                          "failed to alloc at_ on device");
            at_ptrs[cr_id] = d;

            if (Params::Inst()->scattering.dsp.type == "autocorrelate") {
                // Create a DFT plan and store it (as this must be done on the host)
                cufftHandle dft;
                cufftCreate(&dft);
                auto res = cufftPlan1d(&dft, 2 * NF, CUFFT_Z2Z, N);
                if (res != CUFFT_SUCCESS) {
                    sass::err("Could not create a cuFFT plan for "
                              "autocorrelation. cuFFT error code was {}. Program will "
                              "certainly crash.",
                              ( int )res);
                }
                fft_handles[cr_id] = dft;
            }
            ++cr_id;
        }
    }

    timer.start("sd:c:scatter");

    for (size_t n = 0; n < assignment_.size(); n += ATOM_BLOCK) {
        // A CUDA graph is created and executed in multiples of ATOM_BLOCK
        tf::cudaFlow cudaflow;
        // We need to give each graph path/scatter block its own ID for storage of at_ and fft
        // handle
        size_t id = 0;

        for (size_t at = 0; at < std::min(ATOM_BLOCK, assignment_.size() - n); at++) {
            // Create a path in the cudaflow for each of the atoms in the atom block
            for (size_t i = 0; i < NM; i += NTHREADS) {
                // The CUDA kernels are launched in multiples of NTHREADS, corresponding to
                // subvectors
                const size_t N = std::min(NTHREADS, NM - i); // number of subvectors in the block
                const auto atomindex = n + at;

                if (N == 0)
                    break;

                auto zero_dat = cudaflow.memset(at_ptrs[id], 0, 2 * NF * N * sizeof(fftw_complex));

                dim3 single_at_grid((N + blockDim_.x - 1) / blockDim_.x,
                                    (NF + blockDim_.y - 1) / blockDim_.y); // N in the x-axis,
                                                                           // NF in the y-axis.

                auto kernel = cudaflow
                                  .kernel(single_at_grid, blockDim_, 0, sass::cuda::cuda_scatter,
                                          dcoords, dsubvector_index, N, NF, at_ptrs[id],
                                          dscatter_factors, i, atomindex)
                                  .name("self_scatter");

                auto store = cudaflow
                                 .kernel(1, (N + 31) & ~31, 2 * N * sizeof(fftw_complex),
                                         sass::cuda::store, dafinal_, da2final_, at_ptrs[id], N, NF)
                                 .name("store");

                auto store_atfinal =
                    cudaflow
                        .kernel(single_at_grid, blockDim_, 0, sass::cuda::store_atfinal, datfinal_,
                                at_ptrs[id], N, NF)
                        .name("store_atfinal");

                kernel.succeed(zero_dat);
                store.succeed(kernel);
                store_atfinal.succeed(kernel);

                if (Params::Inst()->scattering.dsp.type == "square") {
                    auto dsp = cudaflow
                                   .kernel(single_at_grid, blockDim_, 0,
                                           sass::cuda::square_elements, at_ptrs[id], N, NF)
                                   .name("square_dsp");
                    dsp.precede(store, store_atfinal).succeed(kernel);
                } else if (Params::Inst()->scattering.dsp.type == "autocorrelate") {
                    auto dsp =
                        cudaflow
                            .capture([&](tf::cudaFlowCapturer &cpt) {
                                cpt.on([&](cudaStream_t stream) {
                                    auto dft = fft_handles[id];
                                    cufftSetStream(dft, stream);

                                    // We need twice as many threads because we 2*NF for
                                    // the autocorrelation in the y-axis
                                    dim3 double_at_grid((N + blockDim_.x - 1) / blockDim_.x,
                                                        (2 * NF + blockDim_.y - 1) / blockDim_.y);


                                    auto *a = ( cufftDoubleComplex * )at_ptrs[id];
                                    auto res = cufftExecZ2Z(dft, a, a, CUFFT_FORWARD);
                                    if (res != CUFFT_SUCCESS)
                                        sass::err(
                                            "Error executing forward DFT for autocorrelation.");

                                    sass::cuda::autocorrelate_pow_spect<<<double_at_grid, blockDim_,
                                                                          0, stream>>>(at_ptrs[id],
                                                                                       N, NF);
                                    res = cufftExecZ2Z(dft, a, a, CUFFT_INVERSE);
                                    if (res != CUFFT_SUCCESS)
                                        sass::err(
                                            "Error executing inverse DFT for autocorrelation.");

                                    sass::cuda::autocorrelate_normalize<<<single_at_grid, blockDim_,
                                                                          0, stream>>>(at_ptrs[id],
                                                                                       N, NF);
                                });
                            })
                            .name("autocorrelate_dsp");
                    dsp.precede(store, store_atfinal).succeed(kernel);
                }
                ++id;
                // NOTE: Previously current_subvector would be updated here, but it is not
                // really feasible to do as they are sent to the GPU in blocks of atoms and
                // executed in parallel.
            }
        }

        // Send cudaflow to the gpu and execute
        cudaflow.run(stream);
        stream.synchronize();


        current_atomindex_ += std::min(ATOM_BLOCK, assignment_.size() - n);
        p_monitor_->update(allcomm_.rank(), progress());
    }


    timer.stop("sd:c:scatter");

    // Free the at_ and fft handle for this q-vector
    for (auto &[k, dft] : fft_handles)
        cufftDestroy(dft);
    for (auto &[k, v] : at_ptrs)
        cudaFree(v);



    timer.start("sd:c:wait");
    partitioncomm_.barrier();
    timer.stop("sd:c:wait");

    timer.start("sd:c:reduce");

    // Copy signal outputs back to host
    cudaMemcpy(&afinal_, dafinal_, sizeof(fftw_complex), cudaMemcpyDeviceToHost);
    cudaMemcpy(&a2final_, da2final_, sizeof(fftw_complex), cudaMemcpyDeviceToHost);
    cudaMemcpy(atfinal_, datfinal_, NF * sizeof(fftw_complex), cudaMemcpyDeviceToHost);


    if (NNPP > 1) {
        double *p_atfinal = ( double * )&(atfinal_[0][0]);

        double *p_atlocal = nullptr;
        if (partitioncomm_.rank() == 0) {
            fftw_complex *atlocal_ = ( fftw_complex * )fftw_malloc(NF * sizeof(fftw_complex));
            p_atlocal = ( double * )&(atlocal_[0][0]);
            memset(p_atlocal, 0, NF * sizeof(fftw_complex));
        }

        boost::mpi::reduce(partitioncomm_, p_atfinal, 2 * NF, p_atlocal, std::plus<double>(), 0);
        double *p_afinal = ( double * )&(afinal_);
        std::complex<double> alocal(0);
        double *p_alocal = ( double * )&(alocal);
        boost::mpi::reduce(partitioncomm_, p_afinal, 2, p_alocal, std::plus<double>(), 0);
        double *p_a2final = ( double * )&(a2final_);
        std::complex<double> a2local(0);
        double *p_a2local = ( double * )&(a2local);
        boost::mpi::reduce(partitioncomm_, p_a2final, 2, p_a2local, std::plus<double>(), 0);

        // swap pointers on rank 0
        if (partitioncomm_.rank() == 0) {
            fftw_free(atfinal_);
            atfinal_ = ( fftw_complex * )p_atlocal;
            afinal_ = alocal;
            a2final_ = a2local;
        }
    }

    // norm factor?
    double factor = 1.0 / subvector_index_.size();

    if (partitioncomm_.rank() == 0) {
        // TODO: parallelise?
        smath::multiply_elements(factor, atfinal_, NF);
        afinal_ *= factor;
        a2final_ *= factor;
    }

    cudaFree(dscatter_factors);
    cudaFree(dsubvector_index);

    cudaFree(datfinal_);
    cudaFree(dafinal_);
    cudaFree(da2final_);


    timer.stop("sd:c:reduce");
}


void SelfVectorsCUDAScatterDevice::print_pre_runner_info()
{
    int n_devs = 0;
    cudaGetDeviceCount(&n_devs);

    for (auto i = 0; i < n_devs; ++i) {
        cudaDeviceProp p;
        cudaGetDeviceProperties(&p, i);

        // Taskflow will always pick device number 0
        sass::info("Found CUDA device: {}", p.name);
        sass::info("-> Using device: {}", (i == 0));
        sass::info("-> Total global memory (Gbytes): {:.1f}",
                   ( float )(p.totalGlobalMem) / 1024.0 / 1024.0 / 1024.0);
        sass::info("-> Shared memory per block (Kbytes): {:.1f}",
                   ( float )(p.sharedMemPerBlock) / 1024.0);
    }
}


double SelfVectorsCUDAScatterDevice::progress()
{
    double scale1 = 1.0 / vectors_.size();
    double scale2 = 1.0 / assignment_.size();
    double scale3 = 1.0 / subvector_index_.size();

    double base1 = current_vector_ * scale1;
    double base2 = current_atomindex_ * scale1 * scale2;
    return base1 + base2;
}
// end of file
