/** \file
This file contains a class which implements the scattering calculation for self
scattering.

\author Daniel Vonk <dan@danvonk.com>
\version 1.3.0
\copyright GNU General Public License
*/

#pragma once

// common header
#include "common.hpp"

// special library headers
#include <boost/asio.hpp>
#include <boost/mpi.hpp>
#include <cufft.h>

// other headers
#include "math/coor3d.hpp"
#include "report/timer.hpp"
#include "sample.hpp"

#include "abstract_vectors_xpu_scatter_device.hpp"


/**
Implements self type scattering using vectors for orientational averaging (incoherent scattering)
* Computes scattering amplitudes a_n(q,t)
* Alignment by atoms
*/
class SelfVectorsCUDAScatterDevice : public AbstractVectorsXPUScatterDevice
{
public:
    SelfVectorsCUDAScatterDevice(boost::mpi::communicator allcomm,
                                 boost::mpi::communicator partitioncomm, Sample &sample,
                                 std::vector<CartesianCoor3D> vectors, size_t NAF,
                                 boost::asio::ip::tcp::endpoint fileservice_endpoint,
                                 boost::asio::ip::tcp::endpoint monitorservice_endpoint);

    SelfVectorsCUDAScatterDevice(const SelfVectorsCUDAScatterDevice &other) = delete;
    SelfVectorsCUDAScatterDevice operator=(const SelfVectorsCUDAScatterDevice &other) = delete;
    virtual ~SelfVectorsCUDAScatterDevice();

protected:
    double progress() override;

    void stage_data() override;

    void compute() override;

    bool ram_check() override;


    void print_pre_runner_info() override;

    // calculate amplitudes for a given q and atom for all frames
    fftw_complex *scatter(size_t qindex, size_t aindex) = delete;
    // push count q indexes of (atomindex, q index) to the atscatter queue
    void scatterblock(size_t atomindex, size_t index, size_t count) = delete;


    void store(fftw_complex *at) = delete;
    void dsp(fftw_complex *at) = delete;


    // first = qvec index, second = atomindex
    // Allocated by the data stager (must free ourselves)
    size_t coord_size_ = 0;

    size_t current_atomindex_ = 0;
    ModAssignment assignment_;


private:
    // These values all live on the GPU and mirror the host equivalents
    double *dscatter_factors = nullptr;
    coor_t *dcoords = nullptr;
    CartesianCoor3D *dsubvector_index = nullptr;
    std::complex<double> *dafinal_ = nullptr;
    std::complex<double> *da2final_ = nullptr;
    fftw_complex *datfinal_ = nullptr;

    // Parameters specific to the graphics card we are using
    const size_t NTHREADS = 1024;
    size_t global_memory_ = 0;
    size_t shared_mem_per_block_ = 0;
    int max_threads_per_block_ = 0;
    int max_threads_per_sm_ = 0;
    int num_sm_ = 0;
    int max_parallel_atoms_ = 0;
    dim3 blockDim_ = dim3(32, 32); // a 1024 block
};
