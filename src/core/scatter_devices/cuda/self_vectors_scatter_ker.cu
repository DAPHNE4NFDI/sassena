/** \file
This file contains all the kernels used by the CUDA self scattering device.

\author Daniel Vonk <dan@danvonk.com>
\version 1.3.0
\copyright GNU General Public License
*/

#include <cub/cub.cuh>
#include <cufft.h>
#include <thrust/complex.h>

#include "self_vectors_scatter_ker.cuh"

#include "math/coor3d.hpp"
#include "math/smath.hpp"


#include "common.hpp"


__global__ void sass::cuda::cuda_scatter(coor_t *p_coords, CartesianCoor3D *subvector_index,
                                         size_t N, size_t NF, fftw_complex *at,
                                         double *scatterfactors, size_t index, size_t aindex)
{
    // Calculate the thread index in the 2D grid
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    size_t jdx = blockIdx.y * blockDim.y + threadIdx.y;

    if (idx < N && jdx < NF) {
        size_t offset = idx * 2 * NF;

        const size_t qindex = index + idx; // sub-vector index
        const double s = scatterfactors[aindex]; // b_n

        // output buffer
        fftw_complex *p_at_local = &(at[offset]);

        // q vec
        const double qx = subvector_index[qindex].x;
        const double qy = subvector_index[qindex].y;
        const double qz = subvector_index[qindex].z;

        // r_n
        coor_t *p_data =
            &(p_coords[aindex * NF * 3]); // offset is aindex * framesize * 3-components

        const coor_t x1 = p_data[jdx * 3];
        const coor_t y1 = p_data[jdx * 3 + 1];
        const coor_t z1 = p_data[jdx * 3 + 2];

        // x^T * q
        const double p1 = x1 * qx + y1 * qy + z1 * qz;

        double cp1;
        double sp1;
        sincos(p1, &sp1, &cp1);

        p_at_local[jdx][0] = s * cp1; // re
        p_at_local[jdx][1] = s * sp1; // im
    }
}


// For DSP type "square"
__global__ void sass::cuda::square_elements(fftw_complex *at, size_t N, size_t NF)
{
    // Calculate the thread index in the 2D grid
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    size_t jdx = blockIdx.y * blockDim.y + threadIdx.y;

    if (idx < N && jdx < NF) {
        size_t offset = idx * 2 * NF;

        // Point to the specific element within the subvector
        fftw_complex *data = &(at[offset]);

        double r = data[jdx][0] * data[jdx][0] + data[jdx][1] * data[jdx][1];
        data[jdx][0] = r;
        data[jdx][1] = 0;
    }
}

// For DSP type "autocorrelate"
__global__ void sass::cuda::autocorrelate_pow_spect(fftw_complex *at, size_t N, size_t NF)
{
    // Calculate the thread index in the 2D grid
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    size_t jdx = blockIdx.y * blockDim.y + threadIdx.y;

    if (idx < N && jdx < 2 * NF) {
        size_t offset = idx * 2 * NF;

        // get subvector
        fftw_complex *data = &(at[offset]);

        data[jdx][0] = data[jdx][0] * data[jdx][0] + data[jdx][1] * data[jdx][1];
        data[jdx][1] = 0;
    }
}

__global__ void sass::cuda::autocorrelate_normalize(fftw_complex *at, size_t N, size_t NF)
{
    // Calculate the thread index in the 2D grid
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    size_t jdx = blockIdx.y * blockDim.y + threadIdx.y;

    if (idx < N && jdx < NF) {
        size_t offset = idx * 2 * NF;
        // get subvector
        fftw_complex *data = &(at[offset]);

        double factor = (1.0 / (2.0 * NF * (NF - jdx)));
        data[jdx][0] *= factor;
        data[jdx][1] *= factor;
    }
}

__global__ void sass::cuda::store(fftw_complex *afinal, fftw_complex *a2final, fftw_complex *at,
                                  size_t N, size_t NF)
{
    extern __shared__ thrust::complex<double> shared_mem[];
    thrust::complex<double> *shared_a = shared_mem;
    thrust::complex<double> *shared_a2 = shared_mem + blockDim.x;

    if (threadIdx.x < N) {
        size_t offset = threadIdx.x * 2 * NF;
        // get subvector
        fftw_complex *data = &(at[offset]);

        auto a = thrust::complex<double>(0.0, 0.0);

        for (size_t i = 0; i < NF; i++) {
            a += thrust::complex<double>(data[i][0], data[i][1]);
        }
        a *= 1.0 / NF;

        shared_a[threadIdx.x] = a;
        shared_a2[threadIdx.x] = a * thrust::conj(a);

        auto a2 = a * thrust::conj(a);
    } else {
        shared_a[threadIdx.x] = thrust::complex<double>(0.0, 0.0);
        shared_a2[threadIdx.x] = thrust::complex<double>(0.0, 0.0);
    }

    __syncthreads();

    if (threadIdx.x == 0) {
        thrust::complex<double> block_sum_a(0.0, 0.0);
        thrust::complex<double> block_sum_a2(0.0, 0.0);

        for (size_t i = 0; i < blockDim.x; i++) {
            block_sum_a += shared_a[i];
            block_sum_a2 += shared_a2[i];
        }

        atomicAdd(( double * )&(afinal[0][0]), block_sum_a.real());
        atomicAdd(( double * )&(afinal[0][1]), block_sum_a.imag());
        atomicAdd(( double * )&(a2final[0][0]), block_sum_a2.real());
        atomicAdd(( double * )&(a2final[0][1]), block_sum_a2.imag());
    }
}

__global__ void sass::cuda::store_atfinal(fftw_complex *atfinal, fftw_complex *at, size_t N,
                                          size_t NF)
{
    // Calculate the thread index in the 2D grid
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    size_t jdx = blockIdx.y * blockDim.y + threadIdx.y;

    if (idx < N && jdx < NF) {
        size_t offset = idx * 2 * NF;

        // get subvector
        fftw_complex *data = &(at[offset]);

        atomicAdd(( double * )&(atfinal[jdx][0]), data[jdx][0]);
        atomicAdd(( double * )&(atfinal[jdx][1]), data[jdx][1]);
    }
}
