/** \file
This file contains the interface definition for all scattering devices and
implements an abstract scattering device from which all other devices are
derived.

\author Daniel Vonk <dan@danvonk.com>
\version 1.3.0
\copyright GNU General Public License
*/
#pragma once

#include <memory>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/mpi.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/thread.hpp>
#include <fftw3.h>

#include <taskflow/taskflow.hpp>

#include "common.hpp"

#include "decomposition/assignment.hpp"
#include "i_scatter_device.hpp"
#include "math/coor3d.hpp"
#include "report/timer.hpp"
#include "scatter_factors.hpp"
#include "services.hpp"


/**
Abstract Scattering Device from which all others are derived. It implements
common functionality, e.g. basic control flows.
*/
class AbstractXPUScatterDevice : public IScatterDevice
{
protected:
    coor_t *p_coordinates = nullptr;

    boost::mpi::communicator allcomm_;
    boost::mpi::communicator partitioncomm_;
    Sample &sample_;

    std::vector<CartesianCoor3D> vectors_;
    size_t current_vector_;

    std::unique_ptr<MonitorClient> p_monitor_;
    std::unique_ptr<HDF5WriterClient> p_hdf5writer_;

    // number of nodes, number of frames/time-steps , number of atoms
    size_t NN, NF, NA;

    // temporary storage for reduce & write phase
    fftw_complex *atfinal_ = nullptr;
    std::complex<double> afinal_;
    std::complex<double> a2final_;

    ScatterFactors scatterfactors;

    virtual void stage_data() = 0;
    virtual void compute() = 0;

    /// @brief Compute next Q-vector
    void next();
    void write();

    void runner() override;

    virtual void print_pre_stage_info()
    {
    }
    virtual void print_post_stage_info()
    {
    }
    virtual void print_pre_runner_info()
    {
    }
    virtual void print_post_runner_info()
    {
    }

    virtual bool ram_check();

    /// @brief If there are still Q-vectors left to compute
    /// @return 0 if there are still Q-vectors left to compute, else 1
    bool status() override;
    double progress() override;

    std::map<boost::thread::id, Timer> timer_;

public:
    std::map<boost::thread::id, Timer> &getTimer() override;

    AbstractXPUScatterDevice(boost::mpi::communicator allcomm,
                             boost::mpi::communicator partitioncomm, Sample &sample,
                             std::vector<CartesianCoor3D> vectors, size_t NAF,
                             boost::asio::ip::tcp::endpoint fileservice_endpoint,
                             boost::asio::ip::tcp::endpoint monitorservice_endpoint);
    virtual ~AbstractXPUScatterDevice();

    AbstractXPUScatterDevice(const AbstractXPUScatterDevice &) = delete;
    AbstractXPUScatterDevice &operator=(const AbstractXPUScatterDevice &) = delete;

    void run() override;
};


// end of file
