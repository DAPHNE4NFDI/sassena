/** \file
This file contains an refined version of the abstract scatter device, used for
performing vector based orientationally averaged scattering calculations.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#pragma once

// common header
#include "common.hpp"

// special library headers
#include <boost/mpi.hpp>

// other headers
#include "abstract_scatter_device.hpp"
#include "math/coor3d.hpp"
#include "report/timer.hpp"

/**
Implements control flow for vector based scattering calculations
*/
class AbstractVectorsScatterDevice : public AbstractScatterDevice
{
public:
    AbstractVectorsScatterDevice(boost::mpi::communicator allcomm,
                                 boost::mpi::communicator partitioncomm, Sample &sample,
                                 std::vector<CartesianCoor3D> &&vectors, size_t NAF,
                                 boost::asio::ip::tcp::endpoint fileservice_endpoint,
                                 boost::asio::ip::tcp::endpoint monitorservice_endpoint);

protected:
    size_t NM; // num scattering orientation vectors / sub vectors size

    std::vector<CartesianCoor3D> subvector_index_;
    size_t current_subvector_;

    double progress() override;
    void init_subvectors(CartesianCoor3D &q);

    void print_pre_stage_info() override;
    void print_post_stage_info() override
    {
    }
    void print_pre_runner_info() override;
    void print_post_runner_info() override
    {
    }
};

// end of file
