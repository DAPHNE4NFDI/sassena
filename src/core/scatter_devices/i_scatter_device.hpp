#pragma once

#include <map>

#include <boost/thread.hpp> // TODO: replace with std thread

#include "report/timer.hpp"

/**
Interface class to allow for the execution of the scattering calculation
*/
class IScatterDevice
{
protected:
    virtual void runner() = 0;

    virtual bool status() = 0;
    virtual double progress() = 0;

public:
    // TODO: replace with std thread
    virtual std::map<boost::thread::id, Timer> &getTimer() = 0;
    virtual void run() = 0;
    virtual ~IScatterDevice() {};
};
