#pragma once

#include <string_view>

#include "SassenaConfig.hpp"

namespace sass::platform {
enum class Device
{
    CPU,
    CUDA,
    SYCL
};

constexpr auto device() -> Device
{
#if SASS_USE_CUDA
    return Device::CUDA;
#elif SASS_USE_SYCL
    return Device::SYCL;
#else
    return Device::CPU;
#endif
}

constexpr auto is_cuda() -> bool
{
    return device() == Device::CUDA;
}

constexpr auto is_sycl() -> bool
{
    return device() == Device::SYCL;
}

constexpr auto to_string(Device d) -> std::string_view
{
    using namespace std::literals;
    switch (d) {
    case Device::CPU:
        return "cpu"sv;
    case Device::CUDA:
        return "cuda"sv;
    case Device::SYCL:
        return "sycl"sv;
    }
// TODO: replace with std::unreachable once C++23. The idea is to cause a compile error if a new
// device is added to enum but not here.
#if defined(_MSC_VER) && !defined(__clang__) // MSVC
    __assume(false);
#else // GCC, Clang
    __builtin_unreachable();
#endif
}

}
