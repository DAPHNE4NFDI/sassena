/** \file
This file contains a set of singleton classes, which provide formatted console
outputs.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#ifndef LOG__LOG_HPP_
#define LOG__LOG_HPP_

// common header
#include "common.hpp"

#include <spdlog/spdlog.h>

/**
Singelton class for formatted output to stdout, which should be used for
information type messages.
*/

// TODO: This class can be removed once all calls are replaced with sass::info
class Info
{
private:
    Info() = default;
    Info(const Info &) = default;
    Info &operator=(const Info &)
    {
        return *this;
    }

    static size_t counter;

    std::string prefix;

public:
    static Info *Inst()
    {
        static Info instance;
        counter++;
        return &instance;
    }

    void set_prefix(std::string p)
    {
        prefix = p;
    }

    template<typename... Args>
    inline void write(const char *fmt, Args &&...args)
    {
        spdlog::info(fmt, args...);
    }

    void write(std::string);
    size_t calls()
    {
        return counter;
    }
    ~Info() = default;
};

/**
Singelton class for formatted output to stdout, which should be used for warning
type messages.
*/
// TODO: This class can be removed once all calls are replaced with sass::warn
class Warn
{
private:
    Warn() = default;
    Warn(const Warn &) = default;
    Warn &operator=(const Warn &)
    {
        return *this;
    }

    static size_t counter;
    std::string prefix;

public:
    static Warn *Inst()
    {
        static Warn instance;
        counter++;
        return &instance;
    }

    void set_prefix(std::string p)
    {
        prefix = p;
    }

    template<typename... Args>
    inline void write(const char *fmt, Args &&...args)
    {
        spdlog::warn(fmt, args...);
    }

    void write(std::string);
    size_t calls()
    {
        return counter;
    }
    ~Warn() = default;
};

/**
Singelton class for formatted output to stdout, which should be used for error
type messages.
*/
// TODO: This class can be removed once all calls are replaced with sass::err
class Err
{
private:
    Err() = default;
    Err(const Err &) = default;
    Err &operator=(const Err &)
    {
        return *this;
    }

    static size_t counter;

    std::string prefix;

public:
    static Err *Inst()
    {
        static Err instance;
        counter++;
        return &instance;
    }

    void set_prefix(std::string p)
    {
        prefix = p;
    }

    template<typename... Args>
    inline void write(const char *fmt, Args &&...args)
    {
        spdlog::error(fmt, args...);
    }

    void write(std::string);
    size_t calls()
    {
        return counter;
    }
    ~Err() = default;
};

namespace sass {
template<typename... Args>
inline void info(const char *fmt, Args &&...args)
{
    Info::Inst()->write(fmt, args...);
}

template<typename... Args>
inline void err(const char *fmt, Args &&...args)
{
    Err::Inst()->write(fmt, args...);
}

template<typename... Args>
inline void warn(const char *fmt, Args &&...args)
{
    Warn::Inst()->write(fmt, args...);
}
} // namespace sass



#endif
