/** \file
This file contains a set of singleton classes, which provide formatted console
outputs.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#include "log.hpp"

#include <boost/format.hpp>
#include <spdlog/spdlog.h>

using namespace std;

void Info::write(std::string word)
{
    spdlog::info(word);
}

void Warn::write(std::string word)
{
    spdlog::warn(word);
}

void Err::write(std::string word)
{
    spdlog::error(word);
}

size_t Info::counter = 0;
size_t Warn::counter = 0;
size_t Err::counter = 0;

// end of file
