/** \file
This file contains an efficient timer class, which is used to retrieve execution
times for various parts of the algorithms.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#include "timer.hpp"
#include "control.hpp"
#include "log.hpp"

using namespace std;

double Timer::t_diff(timeval start, timeval end)
{
    int seconds = (end.tv_sec - start.tv_sec);
    int microseconds = (end.tv_usec - start.tv_usec);
    seconds = (microseconds < 0) ? seconds - 1 : seconds;
    microseconds = (microseconds < 0) ? 1000000 + microseconds : microseconds;
    return seconds + (microseconds / 1000000.0);
}

double Timer::t_diff(Timer_timeval start, timeval end)
{
    int seconds = (end.tv_sec - start.tv_sec);
    int microseconds = (end.tv_usec - start.tv_usec);
    seconds = (microseconds < 0) ? seconds - 1 : seconds;
    microseconds = (microseconds < 0) ? 1000000 + microseconds : microseconds;
    return seconds + (microseconds / 1000000.0);
}

void Timer::start(const std::string &tk)
{
    states[tk] = true;

    timeval t {};
    gettimeofday(&t, nullptr);
    starttimes[tk] = t;

    if (Params::Inst()->debug.timer)
        Info::Inst()->write(string("Starting timer for <") + tk + string(">"));
}

void Timer::stop(const std::string &tk)
{
    if (states.find(tk) == states.end())
        return;
    if (states[tk]) {
        states[tk] = false;
        timeval t {};
        gettimeofday(&t, nullptr);
        times[tk](t_diff(starttimes[tk], t));
    } else {
        cerr << "ERROR>> "
             << " Timer not started, but stopped: " << tk << endl;
        throw;
    }
    if (Params::Inst()->debug.timer)
        Info::Inst()->write(string("Stopping timer for <") + tk + string(">"));
}

void Timer::clear()
{
    times.clear();
    starttimes.clear();
    states.clear();
}

vector<string> Timer::keys()
{
    vector<string> ret;
    for (auto &[k, _] : times) {
        ret.push_back(k);
    }
    return ret;
}

double Timer::mean(const std::string &tk)
{
    if (!has_key(tk))
        throw;

    return boost::accumulators::mean(times[tk]);
}

double Timer::variance(const std::string &tk)
{
    if (!has_key(tk))
        throw;

    return boost::accumulators::variance(times[tk]);
}

double Timer::sum(const std::string &tk)
{
    if (!has_key(tk))
        throw;

    return boost::accumulators::sum(times[tk]);
}
double Timer::min(const std::string &tk)
{
    if (!has_key(tk))
        throw;

    return boost::accumulators::min(times[tk]);
}
double Timer::max(const std::string &tk)
{
    if (!has_key(tk))
        throw;
    return boost::accumulators::max(times[tk]);
}

bool Timer::has_key(const std::string &tk)
{
    auto it = times.find(tk);
    return it != times.end();
}
double Timer::count(const std::string &tk)
{
    if (!has_key(tk))
        throw;

    return boost::accumulators::count(times[tk]);
}

// end of file
