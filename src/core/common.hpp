/** \file
The content of this file is included by any other file within the project. Use
it to apply application wide modifications.

\author Benjamin Lindner <ben@benlabs.net>
\version 1.3.0
\copyright GNU General Public License
*/

#pragma once

#include <complex>
#include <vector>

#include "SassenaConfig.hpp"

#ifdef SASS_USE_SINGLE_PRECISION
using coor_t = float;
using coor2_t = double;
using sassf = float;
#else
using coor_t = double;
using coor2_t = double;
using sassf = double;
#endif

constexpr auto sass_use_float()
{
    return std::is_same<sassf, float>::value;
}


double sinc(double x);
