FROM ubuntu:latest
LABEL NAME="sassena-docker-no-cuda" \
        VERSION="1.0" \
        DESCRIPTION="Docker image that contains all required dependencies to build Sassena with CPU support."
RUN DEBIAN_FRONTEND=noninteractive apt-get update -y && apt-get install -y tzdata locales locales-all
RUN apt-get install -y build-essential cmake zlib1g-dev libblas-dev libfftw3-dev libxml2-dev libhdf5-dev \
    wget mpi-default-dev liblapack-dev ninja-build doxygen sphinx-doc
RUN apt-get install -y libspdlog-dev python3 python3-pip graphviz git python3-h5py python3-sphinx-rtd-theme pipx fuse
RUN apt-get install -y curl
RUN pipx install doxysphinx
RUN wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage -O $HOME/.local/bin/linuxdeploy
RUN chmod +x $HOME/.local/bin/linuxdeploy
ENV PATH="$PATH:/root/.local/bin"
RUN wget https://release-cli-downloads.s3.amazonaws.com/latest/release-cli-linux-amd64
RUN mv release-cli-linux-amd64 /usr/local/bin/release-cli
RUN chmod +x /usr/local/bin/release-cli
ARG BOOST_VERSION=1.80.0
RUN cd /tmp && \
    BOOST_VERSION_MOD=$(echo $BOOST_VERSION | tr . _) && \
    wget https://boostorg.jfrog.io/artifactory/main/release/${BOOST_VERSION}/source/boost_${BOOST_VERSION_MOD}.tar.bz2 && \
    tar --bzip2 -xf boost_${BOOST_VERSION_MOD}.tar.bz2 && \
    cd boost_${BOOST_VERSION_MOD} && \
    ./bootstrap.sh --prefix=/usr && \
    echo "using mpi ;" >> project-config.jam && \
    ./b2 install && \
    rm -rf /tmp/*
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
