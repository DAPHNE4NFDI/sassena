Running
-------

Test Project
~~~~~~~~~~~~

Once you have built Sassena, you may wish to try it on a sample project:

.. code-block:: bash

   cd tests/waterdyn4
   ../../build-rel-cpu/bin/sassena --config scatter.xml

The command-line should terminate with something like:

.. code-block:: bash

   [11:38:49 Rank 0] [info] Total runtime (s): 77.552289999999999
   [11:38:49 Rank 0] [info] Successfully finished... Have a nice day!

and will output to ``signal.h5`` by default.

Running with CUDA
~~~~~~~~~~~~~~~~~

Sassena has a CUDA backend for self scattering, though it is only possible
to use it if you have built the project from source and used the "Release
(CPU+CUDA)" or "Dev (CPU+CUDA)" CMake preset. You must direct
Sassena to use it in the ``scatter.xml`` file by adding the following lines inside
the ``<scattering>`` tag:

.. code-block:: xml

   <device>
       <type>cuda</type>
       <atoms>100</atoms>
   </device>

The value in the ``<type>`` tag can be either ``cpu`` or ``cuda`` depending on
the desired backend. If using CUDA, you can dictate how many atoms to calculate
in parallel using the ``<atoms>`` tag. Note that this is subject to memory
capacity on the GPU and will fail if the value is too high. We recommend you
experiment and use a value as high as possible before the program refuses due to
lack of memory.

Sassena can still be run with ``mpirun`` in the usual way when using the CUDA
backend as only one process will claim the GPU.
