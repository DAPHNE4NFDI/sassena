sample
------

.. note::

   sample                                         

   ========== ================= ========= ======= =======
   name       type              instances default allowed
   ========== ================= ========= ======= =======
   structure  sample.structure  0..1      -       -
   framesets  sample.framesets  0..*      -       -
   selections sample.selections 0..*      -       -
   alignments sample.alignments 0..*      -       -
   motions    sample.motions    0..*      -       -
   ========== ================= ========= ======= =======

The sample section contains section which affect the available data.

The sections have the following meaning:

structure
   defines the structural composition of the sample, similar to a
   topology file

framesets
   defines the set of time dependent coordinate files matching the
   structure, i.e. the trajectory

selections
   defines names for sub groups of atoms within the structure

motions
   defines artificial motions which can be applied to groups of atoms

alignments
   defines alignment procedure for groups of atoms which can be applied
   before or after any artificial motions

sample.structure
----------------

.. note::

   sample.structure                                

   ====== ====== ========= ========== ==================
   name   type   instances default       allowed
   ====== ====== ========= ========== ==================
   file   string 0..1      sample.pdb any valid filename
   format string 0..1      pdb        pdb
   ====== ====== ========= ========== ==================

This section contains elements which identify the structure, i.e. the
atomic composition of the sample. The content currently has to be
supplied as a file with PDB format. Only the ATOM entries of the file
are evaluated, other lines are skipped.

.. code-block:: XML

      <structure>
          <file>structure.pdb</file>
          <format>pdb</format>
      </structure>

reads structure information from pdb file “structure.pdb”.

sample.framesets
----------------

.. note::

   sample.framesets                                                         

   ======== ========================= ========= ======= ============
   name     type                      instances default allowed     
   ======== ========================= ========= ======= ============
   first    int                       0..1      0       positive int
   last     int                       0..1      -       positive int
   stride   int                       0..1      1       positive int
   frameset sample.framesets.frameset 0..*      -       -           
   ======== ========================= ========= ======= ============

.. note::

   sample.framesets.frameset

   ====== ====== ========= =============== ==================
   name   type   instances default         allowed
   ====== ====== ========= =============== ==================
   file   string 0..1      sample.dcd      any valid filename
   format string 0..1      dcd             dcd, xtc, trr, pdb
   first  int    0..1      1               positive int
   last   int    0..1      1               positive int
   stride int    0..1      1               positive int
   clones int    0..1      1               positive int
   index  strong 0..1      “file” with tnx any valid filename
   ====== ====== ========= =============== ==================

This section contains an arbitrary number of *frameset* elements. Each
frameset element connect to exactly one file (which can contain an
arbirtrary number of frames). The frameset contains elements
(first,last,stride) which allow the extraction of only a portion of the
contained data. The clones element specifies the number of times, the
current frameset is appended to the internal list of framesets. This, in
combination with the section motions, allows to generate a virtual
trajectory (i.e. trajectory data which does not exist on disk). The
portion specifier elements stride, first and last, can be definied
within the framesets section. If done so, they will be regarded as
default values for each defined frameset element.

.. code-block:: XML

   <framesets>
    <frameset>
     <file>data.dcd</file>
     <format>dcd</format>
     <stride>2</stride>
     <first>10</first>
     <last>100</last>
    </frameset>
   </framesets>

selects frames from a DCD file “data.dcd”, first frame included is
number 11 (indexes start at 0), second frame included is 13 (index
10+2),... no frame with index above 100 is selected.

sample.selections
-----------------

.. note::

   sample.selections                                               

   ========= =========================== ========= ======= =======
   name      type                        instances default allowed
   ========= =========================== ========= ======= =======
   selection sample.selections.selection 0..*      -       -
   ========= =========================== ========= ======= =======

.. note::

   sample.selections.selection

   ========== ====== ========= =========================================== ===========================
   name       type   instances default                                     allowed
   ========== ====== ========= =========================================== ===========================
   type       string 0..1      index                                       index, range, lexical, file
   name       string 0..1      \_#instance                                 any XML text string
   index      int    0..*      -                                           positive int
   from       int    0..1      0                                           positive int
   to         int    0..1      0                                           positive int
   file       string 0..1      selection.pdb                               any valid filename
   format     string 0..1      pdb                                         pdb, ndx
   selector   string 0..1      beta                                        if format=pdb: beta
   selector   string 0..1      beta                                        if format=ndx: name
   expression string 0..1      if type=lexical: ""                         any regular expression
   expression string 0..1      if type=file and format=pdb: 1|1\\.0|1\\.00 any regular expression
   expression string 0..1      if type=file and format=ndx: .*             any regular expression
   ========== ====== ========= =========================================== ===========================

This section contains an arbitrary number of *selection* elements. Each
selection element itself makes names for groups of atoms available.
There are a number of different ways to selection subgroups of atoms.
The type of a selection is specified by the type element. The type then
triggers the parsing of other entries which may be defined. The
selection elements parse the name element to assign labels to the
selections, which can be used in other places to reference the given
subgroup of atoms. If a name is not specified, the generated default
name (“\_#number”) is used as an identifier. If the name can be
determined by other means (e.g. type file, format ndx), then those names
will be used instead.

sample.selections.index
~~~~~~~~~~~~~~~~~~~~~~~

A selection of type index is the most simplistic one. It scans for index
elements and adds them to the named selection. The first atom is
identified by index 0. The last atom is identified by N-1, where N
stands for the number of atoms. You can specify "name" and "index".

.. code-block:: XML

   <selections>
    <selection>
     <name>FourAtoms</name>
     <type>index</type>
     <index>0</index>
     <index>10</index>
     <index>11</index>
     <index>15</index>
    </selection>
   </selections>

creates a selection with name “FourAtoms”, which contains 4 atoms with
indexes 0,10,11 and 15.

sample.selections.range
~~~~~~~~~~~~~~~~~~~~~~~

A ranged selection allows to select a continous group of atoms. This
type of selection is memory efficient, since it only requires the
storage of two indexes. Specify "name", "from", and "to".

.. code-block:: XML

   <selections>
    <selection>
     <name>TenAtoms</name>
     <type>range</type>
     <from>0</from>
     <to>9</to>
   </selection>
   </selections>

creates a selection with name “TenAtoms”, which contains 10 atoms with
indexes between 0 and 9 (inclusive).

sample.selections.lexical
~~~~~~~~~~~~~~~~~~~~~~~~~

A lexical selection allows to select atoms by their internal label,
which is determined by the database. The element expression is used as a
regular expression. All rules regarding regular expressions apply here
(e.g. use “h.\*” instead of “h\*” to select all atoms which start with
h). Specify "name" and "expression".

.. code-block:: XML

   <selections>
    <selection>
     <name>AllHydrogen</name>
     <type>lexical</type>
     <expression>hydro.*</expression>
    </selection>
   </selections>

creates a selection with name “AllHydrogen”, which contains all atoms
with labels starting with “hydro”.

sample.selections.file
~~~~~~~~~~~~~~~~~~~~~~

A file selection allows the definition of more complex selection type
and/or provides a more convenient way of introducing selections. Two
types of formats are currently supported: PDB and NDX. The pdb selection
strategy is similar to the way NAMD allows selections. The NDX format
stands for gromacs style index files. Every selection of type file
allows the specficiation of the selector and expression element. The
selector element is used in combination with the format to indentify
which part in the supplied file contains the selection criterium. The
expression then is evaluated as a regular expression to identify
positives matches. Only those entries which yield a positive match are
added to the internal list of selections. Specify "file", "format",
"selector", and "expression".

.. code-block:: XML

   <selections>
    <selection>
     <name>AtomFromPDB</name>
     <type>file</type>
     <file>selection.pdb</file>
     <format>pdb</format>
     <selector>beta</selector>
     <expression>2|2\.0|2\.00</expression>
    </selection>
   </selections>

creates a selection with name “AtomFromPDB”, which contains all atoms
which have a value in the BETA column matching the regular expression
testing for the number 2.

.. code-block:: XML

   <selections>
    <selection>
     <name>AtomFromNDX</name>
     <type>file</type>
     <file>index.ndx</file>
     <format>ndx</format>
     <selector>name</selector>
     <expression>AGLC.*</expression>
    </selection>
   </selections>

IMPORTS selections from file “index.ndx” (name “AtomFromNDX” is NOT
used!). Only selections with names starting with “AGLC” will be
imported.

sample.motions
--------------

.. note::

   sample.motions                                         

   ====== ===================== ========= ======= =======
   name   type                  instances default allowed
   ====== ===================== ========= ======= =======
   motion sample.motions.motion 0..*      -       -
   ====== ===================== ========= ======= =======

.. note::

   sample.motions.motion

   ========= ====== ========= ============= ===============================================================
   name      type   instances default       allowed
   ========= ====== ========= ============= ===============================================================
   type      string 0..1      linear        fixed, linear, oscillation, randomwalk, brownian, localbrownian
   displace  double 0..1      0.0           any floating point number 
   direction vector 0..1      x=1, y=0, z=0 valid vector defintion    
   selection string 0..1      system        any predefined selection    
   seed      int    0..1      0             positive int 
   sampling  int    0..1      1             positive int 
   frequency double 0..1      0.001         any floating point number 
   radius    double 0..1      displace * 10 any floating point number 
   ========= ====== ========= ============= ===============================================================

This section contains an arbitrary number of *motion* elements. Each
motion element itself applies a type of motion to the reference
selection of atoms. Various types of motions are available. Motions are
applied in the same order as they are defined in the configuration file.
Currently only translational types of motions are supported. Each motion
adds a time-dependent (exception: fixed) position vector
:math:`\vec{r}(t)` to each selected atom. The time :math:`t` is given in
units of frames. Because the user is allowed to change the time unit
easily by applying a stride factor within the frameset section, each
motion allows the definition of an integer *sampling* element. which
should match the stride factor used. This guarantees that the same
displacement is added to the selected frames. The direction of motion is
normalized is any case. For instance, a linear motion with a
displacement :math:`\delta=5` along
:math:`\vec{d}=\left(\begin{array}{ccc}
1 & 1 & 1\end{array}\right)` would yield a position vector of
:math:`\vec{r}=\frac{5}{\sqrt{3}}\left(\begin{array}{ccc}
1 & 1 & 1\end{array}\right)`.

sample.motions.motion.reference
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TODO

::

        motion.reference.type = "instant" or file or frame
        motion.reference.frame = 0
        motion.reference.file
        motion.reference.format
        motion.reference.selection

sample.motions.motion.fixed
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This adds a constant position vector :math:`\vec{r}` to each selected
atom.

.. code-block:: XML

   <motions>
    <motion>
     <type>fixed</type>
     <selection>MotionAtoms</selection>
     <displace>5</displace>
     <direction>
      <x>1</x>
      <y>1</y>
      <z>1</z>
     </direction>
   </motion>
   </motions>

moves atoms within the selection with name “MotionAtoms” by
:math:`\vec{r}(t)=\frac{5}{\sqrt{3}}\left(\begin{array}{ccc}
1 & 1 & 1\end{array}\right)`

sample.motions.motion.linear
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The position vector for linear types of motions is given by
:math:`\vec{r}(t)=\delta\cdot\vec{d}\cdot s\cdot t`. The displacement
:math:`\delta` is given in time units of frames. The direction
:math:`\vec{d}` specifies the normalized base vector. The sampling
factor s allows correction related to the usage of strides. The time
:math:`t` is given in units of frames.

.. code-block:: XML

   <motions>
    <motion>
     <type>linear</type>
     <selection>MotionAtoms</selection>
     <displace>5</displace>
     <direction>
      <x>1</x>
      <y>1</y>
      <z>1</z>
     </direction>
   </motion>
   </motions>

moves atoms within the selection with name “MotionAtoms” by
:math:`\vec{r}(t)=\frac{5}{\sqrt{3}}\left(\begin{array}{ccc}
1 & 1 & 1\end{array}\right)\cdot t`

sample.motions.motion.oscillation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The position vector for oscillations is given by
:math:`\vec{r}(t)=\delta\cdot\vec{d}\cdot\sin\left(2\cdot\pi\cdot f\cdot s\cdot t\right)`,
where the frequency\ :math:`f` is given in units of per frame,
so for example a value of 0.001 corresponds to one full cycle per
1000 frames. The other symbols are similar to the linear type of motion.

.. code-block:: XML

   <motions>
    <motion>
     <type>oscillation</type>
     <selection>MotionAtoms</selection>
     <displace>5</displace>
     <seed>11</seed>
   </motion>
   </motions>

moves atoms within the selection with name “MotionAtoms” by
:math:`\vec{r}(t)=\frac{5}{\sqrt{3}}\left(\begin{array}{ccc}
1 & 1 & 1\end{array}\right)\cdot\sin\left(2\cdot\pi\cdot0.01\cdot t\right)`

sample.motions.motion.random
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The position vector for random motion is given by
:math:`\vec{r}(t)=\sum_{0}^{t}\vec{\delta}(t)`, which corresponds to a
sum of individual increments :math:`\vec{\delta}`. The increments are
generated from a uniform distribution on a 3 dimensional sphere
:math:`P_{Sphere}`. To guarantee that the position vector for two
different times incorporate the same increments for the overlapping time
region, the complete time series of the random motion is computed once
and held in computer memory in form of a motion trajectory. A manual
defintion of a seed value allows the user to reproduce a particular
random motion and to generate different ones, based on needs. The
sampling factor is supported and selects every :math:`s` random
direction. The increments are computed corresponding to
:math:`\vec{\delta}(t)=\delta\cdot\vec{d}_{Sphere}(seed,s\cdot t)`.

.. code-block:: XML

   <motions>
    <motion>
     <type>randomwalk</type>
     <selection>MotionAtoms</selection>
     <displace>5</displace>
     <seed>11</seed>
   </motions>
   </motion>

moves atoms in selection with name “MotionAtoms” by
:math:`\vec{r}(t)=\sum_{0}^{t}5\cdot\vec{\delta}(t)`, with
:math:`\vec{\delta}`\ drawn from uniform distrution on a sphere
(normalized vector). The random generator is initalized with 11.

sample.motions.motion.brownian
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The position vector for brownian motion is similar to random motion with
the exception that the displacement :math:`\delta` is generated from a
gaussian distribution. The corresponding increments are therefore
:math:`\vec{\delta}(t)=\delta_{gaussian}(seed,s\cdot t)\cdot\vec{d}_{Sphere}(seed,s\cdot t)`.

.. code-block:: XML

   <motions>
    <motion>
     <type>brownian</type>
     <selection>MotionAtoms</selection>
     <displace>5</displace>
     <seed>11</seed>
   </motion>
   </motions>

moves atoms within the selection with name “MotionAtoms” by
:math:`\vec{r}(t)=\sum_{0}^{t}5\cdot\vec{\delta}(t)`, with
:math:`\vec{\delta}(t)=\delta_{gaussian}\cdot\vec{d}_{Sphere}`. The
gaussian distribution is seed with 11, the spherical distribution by 12.

sample.motions.motion.localbrownian
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The position vector for localized brownian motion is similar to brownian
motion with the execption that the total displacement :math:`\vec{r}(t)`
is restricted to a value smaller than the radius :math:`R`. The
implementation currently guarantees this by dropping increments which
result in the total displacement to grow beyond :math:`R`. Care must be
taken when using stride factors, since the combination of a restriction
criterium with a sampling factor might break the correct assignment of
random displacements to the original coordinates.

.. code-block:: XML

   <motions>
    <motion>
     <type>fixed</type>
     <selection>MotionAtoms</selection>
     <displace>5</displace>
     <seed>11</seed>
     <radius>15</radius>
   </motion>
   </motions>

moves atoms within the selection with name “MotionAtoms” identical to
the example with brownian motion, but with the additional constrain that
:math:`\left||\vec{r}(t)\right||\leq R` .

sample.alignments
-----------------

.. note::

   ================= =========================== ========= ======= =======
   sample.alignments                                               
   ================= =========================== ========= ======= =======
   name              type                        instances default allowed
   alignment         sample.alignments.alignment 0..\*     -       -
   ================= =========================== ========= ======= =======

.. note::

   sample.alignments.alignment

   ========= ===================================== ========= ======= =====================================
   name      type                                  instances default allowed
   ========= ===================================== ========= ======= =====================================
   type      string                                0..1      center  center, fittrans, fitrot, fitrottrans
   selection string                                0..1      system  any predefined selection
   order     string                                0..1      pre     pre, post
   reference sample.alignments.alignment.reference 0..1      -       -
   ========= ===================================== ========= ======= =====================================

.. note::

   sample.alignments.alignment.reference

   ========= ======= ========= ===================================== =======================
   name      type    instances default                               allowed
   ========= ======= ========= ===================================== =======================
   type      string  0..1      frame                                 frame, file
   frame     integer 0..1      0                                     positive int
   file      string  0..1      sample.structure.file                 any valid filename
   format    string  0..1      sample.structure.format               sample.structure.format
   selection string  0..1      sample.alignments.alignment.selection any valid selection
   ========= ======= ========= ===================================== =======================

Alignments can be applied before (pre) or after (post) the addition of
artifical motions.

sample.alignments.alignment.reference
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TODO

::

        alignment.reference.type = "frame"
        alignment.reference.frame = 0
        alignment.reference.file = sample.structure.file
        alignment.reference.format = sample.structure.format
        alignment.reference.selection = alignment.selection

sample.alignments.alignment.type
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Center alignment computes the center of mass for the given selection of
atoms and moves that center into the origin. The alignment vector is
given by :math:`\vec{d}(t)=\sum_{n}m_{n}\cdot r_{n}(t)/M`, where
:math:`M=\sum_{n}m_{n}` is the total mass of the selection of atoms. The
position of each selected atom is then determined by
:math:`\vec{r}_{n}^{*}(t)=\vec{r}_{n}(t)-\vec{d}(t)`.

.. code-block:: XML

   <alignments>
    <alignment>
     <type>center</type>
     <selection>AlignedAtoms</selection>
     <order>pre</order>
   </alignment>
   </alignments>

moves atoms within the selection with name “AlignedAtoms” by the center
of mass distance *before* any artificial motion is added.

sample.alignments.alignment.fittrans
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Fittrans alignment works like center, but moves the center of mass for
each frame to the center of mass point of a reference frame. The
reference frame can be either supplied by a seperate coordinate file, or
is given a a specified frame number within the trajectory.

sample.alignments.alignment.fitrottrans
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Fitrottrans alignment does least square fitting as practiced by many
other software packages as well. It first performs the same operation as
the Fittrans procedure, but then also removes the rotation of the system
with respect to a reference frame.

.. code-block:: XML

   <alignments>
    <alignment>
     <type>fitrottrans</type>
     <selection>AlignedAtoms</selection>
     <order>pre</order>
     <reference>
      <type>frame</type>
      <frame>0</frame>
      <selection>AlignedAtoms</selection>
     </reference>
   </alignment>
   </alignments>

rotates and translates atoms within the selection with name
“AlignedAtoms” to match the position and orientation of the atoms within
the first frame of the trajectory. Effectivly reduces the dynamics to
internal motion.

sample.alignments.alignment.fitrot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Fitrot alignment does the same as the Fitrottrans operation, but moves
the center of mass back to its original position for each frame. This
procedure removes any rotation of the system, but preserves the
translational motion.
