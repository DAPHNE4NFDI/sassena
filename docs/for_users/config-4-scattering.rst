scattering
----------

.. note::

   ========== ===================== ========= ======= =========
   scattering                                         
   ========== ===================== ========= ======= =========
   name       type                  instances default allowed
   type       string                0..1      all     all, self
   dsp        scattering.dsp        0..1      -       -
   average    scattering.average    0..1      -       -
   vectors    scattering.vectors    0..1      -       -
   background scattering.background 0..1      -       -
   signal     scattering.signal     0..1      -       -
   ========== ===================== ========= ======= =========

The section contains parameters which affect the resulting scattering
signal.

scattering.target
-----------------

obsolete! has been moved to the stager section.

scattering.type
---------------

Two types of scattering functions are currently supported: Coherent
(all) and Incoherent (self) scattering. The calculation schemes for the
two types of scattering are fundamentally different.

.. code-block:: XML

   <type>all</type>

declares that the computed scattering diagram represents coherent
scattering.

scattering.vectors
------------------

.. note::

   scattering.vectors

   ====== ======================== ========= ============= =======================
   name   type                     instances default       allowed
   ====== ======================== ========= ============= =======================
   type   string                   0..1      single        single, scans, file
   single vector                   0..1      x=0, y=0, z=1 valid vector definition
   scans  scattering.vectors.scans 0..1      -             -
   file   string                   0..1      qvectors.txt  any valid filename
   ====== ======================== ========= ============= =======================

.. note::

   scattering.vectors.scans

   ==== ============================= ========= ======= =======
   name type                          instances default allowed
   ==== ============================= ========= ======= =======
   scan scattering.vectors.scans.scan 0..3      -       -
   ==== ============================= ========= ======= =======

.. note::

   scattering.vectors.scans.scan

   ======== ====== ========= ============= =========================
   name     type   instances default       allowed      
   ======== ====== ========= ============= =========================
   from     double 0..1      0             any floating point number 
   to       double 0..1      1             any floating point number 
   points   int    0..1      100           positive int 
   exponent double 0..1      1.0           any floating point number 
   base     vector 0..1      x=1, y=0, z=0 valid vector definition   
   ======== ====== ========= ============= =========================

The scattering diagram contains scattering intensities as a function of
the direction of observation (q vector). This section allows the
defintion of q vectors which are used to compute the scattering diagram.
The vectors type defines how the q vectors are generated/supplied.

scattering.vectors.single
~~~~~~~~~~~~~~~~~~~~~~~~~

When using the type single, the scattering diagram contains only one q
vector.

.. code-block:: XML

   <vectors>
    <type>single</type>
    <x>1</x>
    <y>0</y>
    <z>0</z>
   </vectors>

defines the q vector to be :math:`\vec{q}=(\begin{array}{ccc}
1 & 0 & 0\end{array}` )

scattering.vectors.scans
~~~~~~~~~~~~~~~~~~~~~~~~

The scans type currently allows the definition of up to three scan
elements, which provide ranges of q vectors. The different scan elements
are combined to yield multi-dimensional scans. This may yield a large
number of q vectors. For instance, when defining three scan elements
along the x, y and z axis, respectively, with 100 points each, the total
number of q vectors will be 1000000. Additionally, the exponent element
allows the generation of non-uniform q vector ranges. This is helpful in
cases where some q regions have to be more densly sampled than others. Q
vectors are generated from a range by setting the first and last point
to the supplied from and last. Other q vector values are determined
based on their point assignment :math:`i`:
:math:`q_{i}=(\frac{i}{N})^{E}\cdot(q_{to}-q_{from})`. The direction of
each q vector is determined by the supplied base vectors (normalized).

.. code-block:: XML

   <vectors>
    <type>scans</type>
    <scans>
     <scan>
     <x>1</x><y>0</y><z>0</z>
     <from>-2</from>
     <to>2</to>
     <points>50</points>
     </scan>
     <scan>
      <x>0</x><y>1</y><z>0</z>
     <from>-2</from>
     <to>2</to>
     <points>50</points>
     </scan>
    </scans>
    </vectors>

creates a list of q vectors, corresponding to a scattering diagram in
the xy plane. The resulting diagram has a 50 pixel resolution in each
direction with 49 steps of 4/49 from -2 to 2.

.. code-block:: XML

   <vectors>
    <type>scans</type>
    <scans>
     <scan>
     <x>1</x><y>0</y><z>0</z>
     <from>0.001</from>
     <to>1</to>
     <points>100</points>
     <exponent>3</exponent>
     </scan>
    </scans>
    </vectors>

creates a list of q vectors, corresponding to a scattering diagram in
the along the x axis. The resulting diagram has a 100 pixel resolution
99 steps. The step size is non-linear with an exponent of 3.

scattering.vectors.file
~~~~~~~~~~~~~~~~~~~~~~~

The file type allows to read q vectors from a source text file (
line-by-line, whitespace delimited). This way the user can use their own
algorithms to generate complex sets of q vectors.

.. code-block:: XML

   <vectors>
    <type>file</type>
    <file>qvectors.txt</file>
   </vectors>

creates 5 q vectors based on the contents in file “qvectors.txt”.

::

   1 0 0
   1 1 1
   1 0 1
   0 1 0
   0.3 0.3 0.1

scattering.average
------------------

.. note::

   scattering.average

   =========== ============================== ========= ======= =======
   name        type                           instances default allowed
   =========== ============================== ========= ======= =======
   orientation scattering.average.orientation 0..1      -       -      
   =========== ============================== ========= ======= =======

.. note::

   scattering.average.orientation

   ========= ======================================== ========= ======= ==================
   name      type                                     instances default allowed
   ========= ======================================== ========= ======= ==================
   type      string                                   0..1      vectors vectors, multipole
   vectors   scattering.average.orientation.vectors   0..1      -       -
   multipole scattering.average.orientation.multipole 0..1      -       -
   ========= ======================================== ========= ======= ==================

.. note::

   scattering.average.orientation.vectors

   ========== ====== ========= ======================== ========================================================
   name       type   instances default                  allowed
   ========== ====== ========= ======================== ========================================================
   type       string 0..1      sphere                   sphere, cylinder, file
   algorithm  string 0..1      boost_uniform_on_sphere  if type=sphere: boost_uniform_on_sphere
   algorithm  string 0..1      boost_uniform_on_sphere  if type=cylinder: boost_uniform_on_sphere, raster_linear
   file       string 0..1      qvector-orientations.txt any valid filename
   seed       int    0..1      0                        positive int
   resolution int    0..1      100                      positive int
   axis       vector 0..1      x=0, y=0, z=1            valid vector definition
   ========== ====== ========= ======================== ========================================================

.. note::

   scattering.average.orientation.multipole

   ========== ====== ========= ============= =======================
   name       type   instances default       allowed
   ========== ====== ========= ============= =======================
   type       string 0..1      sphere        sphere, cylinder
   resolution int    0..1      20            positive int
   axis       vector 0..1      x=0, y=0, z=1 valid vector definition
   ========== ====== ========= ============= =======================

This section defines the type of averaging procedures which are applied
in-place. Currently only orientational averaging is supported. There are
two types of orientational averaging procedures (Monte Carlo,
Multipole). The Monte Carlo scheme performs oriental averaging by
recomputing and integrating the scattering signal for a set of random
directions. This corresponds to stochastic integration, which is
generally good when the intensities do not vary signficantly. For highly
crystalline samples, which feature strong Bragg peaks, a large number of
vectors may be necessary to reach convergence ( O(5)-O(6) ). The
Multipole scheme employs a multipole expansion of the exponential terms
involved in the scattering calculation. It performs superior for low q
values ( q<<1 ). At large q values, multipole moments of high order
become dominant, which requires to increase resolution incrementally.

scattering.average.vectors
~~~~~~~~~~~~~~~~~~~~~~~~~~

The vectors type triggers the Monte Carlo scheme for orientational
averaging and the parsing of the vectors sections in
scattering.average.orientations. The q vector orientations are
determined in one of three ways. When using file type, the orientations
are determined from a file, similar to “qvectors.txt” in section
scattering.vectors.file. The other two methods allow spherical or
cylindrical averaging using an internal algorithm to generate random
orientations. The resolution specifies the number of directions which
contribute to the integral of the orientationally averaged scattering
intensity. In each case, the q vector length is taken from the original
q vector. When computing orientationally averaged scattering diagrams
with more than one q vector, the same set of random orientations is used
in each case.

.. code-block:: XML

   <average>
    <orientation>
     <type>vectors</type>
     <vectors>
      <type>sphere</type>
      <algorithm>boost_uniform_on_sphere</algorithm>
      <resolution>1000</resolution>
      <seed>5</seed>
     </vectors>
    <orientation>
   </average>

triggers isotropic (sphere) orientational averaging, using 1000 random
directions and a seed value of 5 for the random number generator.

.. code-block:: XML

   <average>
    <orientation>
     <type>vectors</type>
     <vectors>
      <type>cylinder</type>
      <algorithm>boost_uniform_on_sphere</algorithm>
      <resolution>1000</resolution>
      <seed>5</seed>
      <axis>
       <x>1</x><y>1</y><z>1</z>
      <axis>
     </vectors>
    <orientation>
   </average>

triggers anisotropic (cylindrical) orientational averaging, using 1000
random directions and a seed value of 5 for the random number generator.
The cylinder axis points towards :math:`(\begin{array}{ccc}
1 & 1 & 1\end{array}).`

scattering.average.multipole
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TODO

::

	        multipole
                type = "sphere"
	            moments
                    type
                    resolution = 20
                    file = "moments.txt"
	        axis
                x = 0
                y = 0
                z = 1.0
	        exact
                type = "sphere"

scattering.dsp
--------------

.. note::

   scattering.dsp

   ====== ====== ========= ============= ============================
   name   type   instances default       allowed
   ====== ====== ========= ============= ============================
   type   string 0..1      autocorrelate autocorrelate, square, plain
   method string 0..1      fftw          direct, fftw
   ====== ====== ========= ============= ============================

In the first stage the software computes complex scattering amplitudes.
For coherent scattering (all) the results on each nodes are then
communicated and gathered on a selected node. For incoherent scattering
(self) this is not necessary. The aggregated data corresponds to the
full time series of the complex scattering amplitue for the system and
the individual atoms, for coherent and incoherent scattering,
respectively. At this stage, the user may employ a time series analysis
or manipulation of the data. Currently two types of routines are
implemented, one computing the autocorrelation and the other one doing
an element-wise complex conjugate multiplication.

scattering.dsp.type.autocorrelate
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When using the dsp type autocorrelate, the signal is replaced by its
autorrelation. Autocorrelation can be either computed with a direct
algorithm or with fftw routines. The fftw routines usually feature a
superior scaling for large number of timesteps.

.. code-block:: XML

   <dsp>
    <type>autocorrelate</type>
    <method>fftw</method>
   </dsp>

will trigger the autocorrelation of the scattering signal. The fftw
method is used.

scattering.dsp.type.square
~~~~~~~~~~~~~~~~~~~~~~~~~~

When using the dsp type square, each element of the signal is multiplied
with its conjugate complex value. The resulting signal is pure real and
can be regarded as the time series of the zero time delay value of the
signal autocorrelation.

.. code-block:: XML

   <dsp>
    <type>square</type>
   </dsp>

triggers the squaring of the scattering signal. The resulting signal
will be purely real valued.

scattering.signal
-----------------

.. note::

   scattering.signal                        

   ==== ====== ========= ========= ===========
   name type   instances default   allowed
   ==== ====== ========= ========= ===========
   file string 0..1      signal.h5 any valid filename
   fqt  bool   0..1      true      true, false
   fq0  bool   0..1      true      true, false
   fq   bool   0..1      true      true, false
   fq2  bool   0..1      true      true, false
   ==== ====== ========= ========= ===========

Each q vector yields a complete time series of the scattering intensity
with the exact form depending on any settings in the dsp section. For
some scattering calculations the time dependent information can be
eliminated and replaced by a mean value, i.e. the scattering diagram
becomes a function of only the q vector. In that case the total
time-dependent signal “fqt” may be discarded and only some aspects of
this function be preserved. Currently 3 additional values are computed
for each “fqt”: “fq” which is the total time integral of “fqt”, “fq0”
which is the zero-time element (for correlation, the zero time delay
element) of “fqt” and “fq2”, which corresponds to the complex conjugate
multiplication of “fq”. Whether or not these data element are written to
the final signal file, can be triggered by activating their
corresponding values. The default is that each value is written to the
output signal file. The output file is written in the hdf5 format.

.. code-block:: XML

   <signal>
    <file>mysignal.h5</file>
    <fqt>false</fqt>
    <fq0>true</fq0>
    <fq>true</fq>
    <fq2>false</fq2>
   </signal>

will write dataset entries for fq0 and fq, but not for fqt and fq2. The
output ist stored in hdf5 format in the file with name “mysignal.h5”.

scattering.background
---------------------

.. note::

   scattering.background

   ====== ============================ ========= ======= =========================
   name   type                         instances default allowed
   ====== ============================ ========= ======= =========================
   factor double                       0..1      0       any floating point number
   kappas scattering.background.kappas 0..1      -       -
   ====== ============================ ========= ======= =========================

.. note::

   scattering.background.kappas

   ===== ================================== ========= ======= =======
   name  type                               instances default allowed
   ===== ================================== ========= ======= =======
   kappa scattering.background.kappas.kappa 0..1      -       -
   ===== ================================== ========= ======= =======

.. note::

   scattering.background.kappas.kappa

   ========= ====== ========= ======= =========================
   name      type   instances default allowed
   ========= ====== ========= ======= =========================
   selection string 0..1      system  any predefined selection
   value     double 0..1      1.0     any floating point number
   ========= ====== ========= ======= =========================

Each atom has an assigned atomic scattering length. In case of x-ray
scattering, the scattering length depends on the q vector length. Since
the scattering from molecular structure data only incorporates atoms
which are explicitly modeled, the final scattering diagram is missing
scattering from the surrounding and the solvent. For small values of q,
the surrounding can be approximated by substracting an effective
scattering length density of the typical system from the individual
atomic scattering lengths. The correction requires to approximate the
excluded volume effect of the particular atom. One of two major
contributions comes from the size of the particular atom, the other from
the molecular phase it is incorporated in. The database defines excluded
volumes for common atom types. Additionally the user may scale these
volumes dependent on the particular material the atoms are incorporated
in by specifying a kappa value (scaling coefficient) and the respective
atom selection. For instance an oxygen atom within water may displace
more volume than an average oxygen atom within a protein.

A major idea of this type of correction is that the scattering of a
disordered system, e.g. water, should not produce a scattering intensity
for low q values (q=0). However, the scattering calculation of a finite
box of water will result in a non-zero scattering intensity at low q
values, which is an artefact due to the missing surrounding. The
surrounding can be approximated by offsetting the individual atomic
scattering lengths so that the overall scattering becomes zero at low q
values.

.. code-block:: XML

   <background>
    <factor>0.005</factor>
    <kappas>
     <kappa>
      <selection>Water</selection>
      <value>1.42</value>
     </kappa>
    </kappas>
   </background>

set the background scattering length density to 0.005 and scales the
volumes for atoms incorpated in the selection “Water” by a factor of
1.42.
