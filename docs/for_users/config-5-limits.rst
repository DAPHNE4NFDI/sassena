limits
------

.. note::

   limits                                               

   ============= ==================== ========= ======= =======
   name          type                 instances default allowed
   ============= ==================== ========= ======= =======
   stage         limits.stage         0..1      -       -
   signal        limits.signal        0..1      -       -
   computation   limits.computation   0..1      -       -
   services      limits.services      0..1      -       -
   decomposition limits.decomposition 0..1      -       -
   ============= ==================== ========= ======= =======

The parameters specified in the limits section allow to adjust threshold
values and performance figures. Threshold values exist to guarantee that
the software does not crash due to resource starvation. It also protects
the compute nodes from abusive configurations. However, the threshold
value might not fit any possible use case, in which cases the user may
may want to overwrite theses values. Some values have limited lifetime
within the application and/or limited scope. The section limits is
organized into contexts. Each context has a certain lifetime during the
application, see Figure X for details. Parameters are usually declared
in the context in which they are instantiated. However, the lifetime of
the particular parameter may exceed the lifetime of the context (e.g.
setting the buffer size for coordinates during staging, which will
remain during the computation). The default values are tuned to allow
for a wide range of use case and applicability on the state-of-the art
cluster designs. When changing the default values, the user should take
care to guarantee that the available hardware resources match the
computational requirements.

limits.stage
------------

.. note::

   limits.stage                                       

   ====== =================== ========= ======= =======
   name   type                instances default allowed
   ====== =================== ========= ======= =======
   memory limits.stage.memory 0..1      -       -
   ====== =================== ========= ======= =======

.. note::

   limits.stage.memory                          

   ====== ==== ========= ================= ============
   name   type instances default           allowed
   ====== ==== ========= ================= ============
   data   int  0..1      524288000 = 500MB positive int
   buffer int  0..1      104857600 = 100MB positive int
   ====== ==== ========= ================= ============

Before any computation is performed, the cartesian coordinates are read
into local memory. This staging of the data is split into two phases.

In the first phase, the first partition reads the trajectory data from
the storage device (disk,network) and stores them into the internal
buffer for the coordinates data (limits.stage.memory.data). When
computing coherent scattering, the data alignment in the trajectory
files coiincites with the partitioning scheme, allowing each node to
read the coordinates directly into the local buffer. For incoherent
scattering, the data has to be aligned by atoms, thus requiring a
tranposition of the data during the initial read. This requires
additional buffers (limits.stage.memory.buffer). The transposition of
the data is carried out through a collective MPI all-to-all, which
results in a synchronization point. To minimize the number of
synchronization points, the internal buffer is filled completely, before
the data gets transposed. The internal buffer has to have a minimum size
to hold at least one frame of the data. The size of the partition
determines the number of nodes which access the trajectory data in
parallel, thus increasing the partition size results in a more
aggressive IO behavior.

In the second phase, the coordinates stored in the first partition are
cloned to all other partitions. This is implemented through the MPI
collective broadcast.

.. code-block:: XML

   <stage>
    <memory>
     <data>600000000</data>
     <buffer>50000000</buffer>
    </memory>
   </stage>

The available memory for the local storage of coordinates is set to
about 600MB. The buffer during data exchange is about 50MB.

limits.signal
-------------

.. note::

   limits.signal                        

   ========= ==== ========= ======= ============
   name      type instances default allowed
   ========= ==== ========= ======= ============
   chunksize int  0..1      10000   positive int
   ========= ==== ========= ======= ============

The output file is written in hdf5 format. Parameters which are related
to the content of the signal file are given in scattering.signal. The
parameters in this section (limits.stage) determine “how” the signal
file is written. This can affect overall performance. The default
parameters should yield good performance in most cases. Currently, only
the chunksize parameter is adjustable. It determines the minimum size of
a data element existiting on the disk. Please refer to the HDF5 manual
for details on chunks. The default value is 10000 (corresponds to
complex value entries, e.g. 16 bytes each). For the “fqt” signal, the
cunks are aligned the time dimension. If the time dimension has
significantly less than 10000 entries, the chunks contain more than one
q vector. In general large chunksizes are prefered for large datasets,
because each chunk element has to be managed within the HF5 file.
Millions of chunks may slow down the reading and writing of the data
considerably. For acceptable performance, the number of chunk elements
should be kept to be smaller than 50000. With a default chunksize of
10000 (160kbyte) this corresponds to a file size of 8GB. If larger
datasets have to be stored, the chunksize should be increased.

.. code-block:: XML

   <signal>
    <chunksize>20000</chunksize>
   </signal>

doubles the chunksize

limits.computation
------------------

.. note::

   limits.computation

   ======= ========================= ========= ======= ============
   name    type                      instances default allowed
   ======= ========================= ========= ======= ============
   memory  limits.computation.memory 0..1      -       -
   threads int                       0..1      1       positive int
   ======= ========================= ========= ======= ============

.. note::

   limits.computation.memory                          

   =============== ==== ========= ================= ============
   name            type instances default           allowed
   =============== ==== ========= ================= ============
   signal_buffer   int  0..1      104857600 = 100MB positive int
   result_buffer   int  0..1      104857600 = 100MB positive int
   exchange_buffer int  0..1      104857600 = 100MB positive int
   alignpad_buffer int  0..1      209715200 = 200MB positive int
   scale           int  0..1      1                 positive int
   =============== ==== ========= ================= ============

During the calculation the incoherent (self) and coherent (all)
scattering, the total time signal for each q vector orientation has to
be aggregated on one node. Also, each node has to keep a local cache of
the total time signal to avoid unneccessary communication. This can
consume a considerable amount of computer memory, which might lead to
resource starvation. The parameter in section computation.memory
protects the user from accidental memory overconsumption. The current
defaults allows for storage of up to 4 million time steps (frames). If
longer trajectories have to be examined and the necessary hardware
requirements are met, adjusting the parameters under computation.memory
allows for an arbitrary long time signal. The software also has
experimental support for threads. By default only one worker thread per
MPI node is active. Setting computation.threads to higher values allows
the use of multiple threads to utilize local parallelism. To avoid
synchronization between the threads, each thread has own its own memory
space. Thus the use of threads may be memory limited. The utility of
threads is scoped to averaging prodecures, i.e. it enables parallelism
for the computation of orientational averages. Not all buffers are used
at the same time and by all modes. Sassena has a memory check routine
which anticipates the memory use and provides guidance on the
recommended limits. It is thus recommended to only increase the limits
if the software asks for it. The convience parameter “scale” provides a
means to simply increase the memory limits by the specified factor,
which is useful for underallocation of compute nodes for the sake of
providing more memory per MPI process (e.g. a 12-core computer node may
have 12GB RAM. Allocating 12 MPI processes would provide a maximum of
1GB memory to each process. If allocating 3 MPI processes we allow for
4GB per process and simply increase the sassena software memory limits
by setting “scale” to 4.)

.. code-block:: XML

   <computation>
    <memory>
     <signal_buffer>200000000<signal_buffer>
     <result_buffer>200000000<result_buffer>
     <exchange_buffer>200000000<exchange_buffer>
     <alignpad_buffer>200000000<alignpad_buffer>
     <scale>4</scale>
    </memory>
    <threads>4</threads>
   </computation>

increases the internal memory threshold for each buffer to ~800MB. Also
the number of worker threads is set to 4.

limits.services
---------------

.. note::

   limits.services                                          

   ====== ====================== ========= ======= =======
   name   type                   instances default allowed
   ====== ====================== ========= ======= =======
   signal limits.services.signal 0..1      -       -
   ====== ====================== ========= ======= =======

.. note::

   limits.services.signal

   ====== ============================= ========= ======= =======
   name   type                          instances default allowed
   ====== ============================= ========= ======= =======
   memory limits.services.signal.memory 0..1      -       -
   times  limites.services.signal.times 0..1      -       -
   ====== ============================= ========= ======= =======

.. note::

   limits.services.signal.memory                          

   ====== ==== ========= ========= ============
   name   type instances default   allowed
   ====== ==== ========= ========= ============
   server int  0..1      104857600 positive int
   client int  0..1      10485760  positive int
   ====== ==== ========= ========= ============

.. note::

   limits.services.signal.times                        

   =========== ==== ========= ======= ============
   name        type instances default allowed
   =========== ==== ========= ======= ============
   serverflush int  0..1      600     positive int
   clientflush int  0..1      600     positive int
   =========== ==== ========= ======= ============

To avoid synchronization points between the partitions when writing data
to the output file and when reporting progress to the console, the
necessary services have been implemented as seperate network protocols.
When the software starts up, it initializes the services and starts the
corresponding threads. The server threads are located on MPI node rank
0. Each MPI node then sends signal output and progress information via
these interfaces. These interfaces bind to the tcp ethernet. This is OK,
since the amount of progress information and the final signal data fits
well within the capacities of gigabit ethernets. The effect of network
latencies is reduced by the implementation of signal output buffers,
which can be adjusted by the parameters in
limits.services.signal.memory. The parameters in
limits.services.signal.times allows to specificy time intervals in
seconds for which the signal data should be communicated an written to
disk. Using the MPI layer for progress and signal output data, would
allow better performance, however it requires to dedicate MPI nodes
(threading support for MPI is still not supporting on all machines).

.. code-block:: XML

   <services>
    <signal>
     <memory>
      <server>30000000</server>
      <client>2000000</client>
     </memory>
     <times>
      <serverflush>300</serverflush>
      <clientflush>300</clientflush>
     </times>
    </signal>
   </services>

sets the signal output buffer sizes to about 30MB for the server and 2MB
for the clients. The timeouts for flushing data to disk or to the server
is set to 300 seconds for the server and the client, respectivley.

limits.decomposition
--------------------

The efficient utilization of the parallel environment requires the
partitioning of the problem based on some metrics. For coherent (all)
scattering the best partitioning strategy is frame based, for incoherent
(self) it is atom based. However, the number of frames and atoms may
limit the scalability. In that case more than one q vector may be
processed in parallel. The software uses a heuristic deterministic
partitioning scheme to calculate absolute utilization factors. It will
find the parititoning with the global best utilization. When more than
one best solution (utilization) is available, the paritioning algorithm
favors large parititons. The user may want to manually specifiy the
partition size by setting limits.decomposition.partitions.automatic to
false and set the partition size with
limits.decomposition.partitions.size. This may be necessary when a
pariticular partitioning is favored, e.g. to match the number of cores
per machine with the partition size, thus eliminating inter-node
communication. The algorithm does not take this into account. Another
reason to fix the partition size is to achieve a specific IO
performance, since the parallel bandwidth (number of nodes which read
the trajectory) during the staging is determined by the partition size.

.. code-block:: XML

   <decomposition>
    <partitions>
     <automatic>false</automatic>
     <size>8</size>
    </partitions>
   </decomposition>

disables automatic decomposition and set the partition size to 8. Given
that at least as many q vectors are to be computed, this yields a total
of 5 active partitions when using 40 nodes.
