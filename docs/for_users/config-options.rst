*******************
Config file options
*******************

.. note::

   root                                    

   ========== ========== ========= ======= =======
   name       type       instances default allowed
   ========== ========== ========= ======= =======
   sample     sample     0..1      -       -
   stager     stager     0..1      -       -
   scattering scattering 0..1      -       -
   limits     limits     0..1      -       -
   debug      debug      0..1      -       -
   ========== ========== ========= ======= =======

Some sections may require an element of type vector:

.. note::

   vector                          

   ===== ====== ========= ======= =========================
   name  type   instances default allowed
   ===== ====== ========= ======= =========================
   x     double 0..1      0       any floating point number
   y     double 0..1      0       any floating point number
   z     double 0..1      0       any floating point number
   ===== ====== ========= ======= =========================

The Sassena configuration file enables the user to set mandatory and
optional parameter which are used during execution of the software. Each
optional configuration parameter is supplied with a default value. The
configuration file is based on the XML format. The configuration
parameters are organized into a tree hierarchy which maps a class
inheritance diagram.

The configuration file is parsed for appearances of certain key
sections. Sections which do not map to a valid entry are currently
ignored. *The user should be aware that misspellings of sections which
are not mandatory may result in those sections to exercise their default
behavior.*

Some entries trigger the parsing of other sections. If that is the case,
the user has to make sure that these sections are properly defined.

The configuration file is structured into 6 main sections.

Some sections may contain multiple sections with identical names. In
this case the order by which they appear in the file is preserved.

The four different sections have the following distinct features:

sample
======

contains sections which modify the available data. This includes
coordinates and selection names.

.. toctree::
   :maxdepth: 1
   
   config-1-sample

stager
======

contains sections which determines the staging mode of the trajectory
data.

.. toctree::
   :maxdepth: 1

   config-2-stager

database
========

contains sections relevant for selecting the correct database

.. toctree::
   :maxdepth: 1

   config-3-database

scattering
==========

contains sections which modify the retrieved signal, but don’t affect
the available data.

.. toctree::
   :maxdepth: 1

   config-4-scattering

limits
======

contains sections which neither modify the retrieved signal, nor the
available data, but impacts the performance and computational aspects
on how the calculation is carried out.

.. toctree::
   :maxdepth: 1

   config-5-limits

debug
=====
contains sections which provide control switches and debug
information for different aspects of the software. This section is
intended to allow debugging without the need for recompilation. It
may affect available data, the retrieved signal and the achieved
performance

.. toctree::
   :maxdepth: 1

   config-6-debug
