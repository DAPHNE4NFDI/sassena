stager
------

.. note::

   ====== ====== ========= ======== =========================
   stager                           
   ====== ====== ========= ======== =========================
   name   type   instances default  allowed
   target string 0..1      system   any valid selection
   mode   string 0..1      frames   frames, atoms
   dump   bool   0..1      false    true, false
   file   string 0..1      dump.dcd any valid filename string
   format string 0..1      dcd      dcd
   ====== ====== ========= ======== =========================

This section mainly effects the staging procedure. However, the
scattering type (all/self) may enforce a specific staging mode. The
seperation of the staging mode into its own section allows future
extension of the software towards other uses and allows the use of the
data staging modes without analysis for distinct purpose (the tool
s_stager simply stages data without analysis), e.g. parallel reading and
processing of the trajectory data with a subsequent parallel write, or
the inversion of the trajectory data layout (atoms <-> frames).

stager.target
-------------

This section allows the definition of a target selection. This enables
the user to compute the scattering or perform an analysis on a subgroup
of atoms without the need of producing a reduced trajectory. The default
is to include all atoms (system). The selection has to be defined within
the section sample.selections.

.. code-block:: XML

   <target>SelectedAtoms</target>

declares that only atoms of the selection with name “SelectedAtoms” are
considered when computing the scattering diagram.

stager.mode
-----------

Mode can be either “atoms” or “frames”. The mode is usually enforced by
the specific analysis (thus overwritten). However, data processing which
only operates on the trajectory data (like the tool s_stager), requires
the specification of the proper staging mode. Mode “frames” distributes
complete frames among the available nodes, while “atoms” assigns atoms
to nodes.

.. code-block:: XML

   <mode>frames</mode>

declares that the data will be staged by frames, UNLESS the analysis
enforces a specific staging mode.

stager.dump
-----------

The in-memory trajectory data can be written to a new file. This allows
the use of the sassena package to extract and post-process trajectory
data efficiently. If the target only specified a sub-selection of atoms
in the system, then the written trajectory only contains those.

.. code-block:: XML

   <dump>true</dump>
   <file>dump.dcd</file>
   <format>dcd</format>

writes the in-memory trajectory data to the file dump.dcd in DCD format.
If the staging mode is “atoms” the trajectory is in effect transposed,
which means that the first frame of the new trajectory contains the
positions of the first atom, the second frame contains all positions of
the second atom, and so on.
