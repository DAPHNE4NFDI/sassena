database
--------

.. note::

   ======== ====== ========= ======= =========================
   database                          
   ======== ====== ========= ======= =========================
   name     type   instances default allowed
   type     string 0..1      file    file
   file     string 0..1      db.xml  any valid filename string
   format   string 0..1      xml     xml
   ======== ====== ========= ======= =========================

This section contains parameters affecting database selection. A
database selection has 3 attributes: type, type-specific locator,
type-specific format. Currently only the file type is supported.

.. code-block:: XML

	<database>
   <type>file</type>
   <file>db-special.xml</file>
   <format>xml</format>
  </database>

selects database parameters from the xml file db-special.xml
