Command line options
--------------------

Sassena is configured by an xml configuration file.
By default, Sassena will look for a file scatter.xml
in the current directory. Some settings of the
configuration file can be overridden by command line
options.

``--help``
	show help message

``--config=FILENAME``
	xml configuration file name (default: ./scatter.xml)

``--sample.structure.file=FILENAME``
	Structure file name (default: ./sample.pdb)

``--sample.structure.format=FORMAT``
	Structure file format (default: pdb)

``--stager.file=FILENAME``
	Trajectory file name (default: ./dump.dcd)
		
``--stager.format=FORMAT``
	Trajectory file format (default: dcd)
    
``--scattering.signal.file=FILENAME``
	Output file name (default: ./signal.h5)
    
``--stager.target=SELECTION``
	Atom selection producing the signal (default: system)
    
``--stager.dump=BOOL``
	Do/Don't dump the postprocessed coordinates to a file (default: false)
    
``--limits.computation.threads=NUMBER``
	Number of threads per process (default: 1)
