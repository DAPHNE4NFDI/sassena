Installation
------------

AppImage
~~~~~~~~

An AppImage is provided that can run on Linux (x64, without CUDA support) out of
the box when executed (no installation needed). To use it, download the latest AppImage
from the `releases page
<https://codebase.helmholtz.cloud/DAPHNE4NFDI/sassena/-/releases>`_, mark it as
an executable and run it from the command-line:

.. code-block:: bash

   chmod +x Sassena.AppImage
   ./Sassena.AppImage # --config scatter.xml etc.
   # or to use mpi:
   mpirun -n 16 ./Sassena.AppImage --config scatter.xml

Compiling from Source
~~~~~~~~~~~~~~~~~~~~~

If you want to target a specific instruction set (e.g. ARM) or want to use the
CUDA implementation, compiling from source is needed.

VSCode
""""""

The easiest way to compile Sassena is by using VSCode's `Dev Containers
<https://code.visualstudio.com/docs/devcontainers/containers>`_. This will
automatically create a Docker container, install dependencies, configure the build and compile Sassena
inside that container. Sassena must then be run form inside this container. This
means that no additional dependencies (e.g. Boost) need to be installed on the
system as they are all encapsulated inside the container.

To use VSCode's Dev Containers, you need to have *Docker* installed. For Linux,
this means Docker Engine and Docker Compose. Please follow the installation
guide for Docker `here <https://docs.docker.com/engine/install/>`_ and note that
you must add your user to the `docker` group so that VSCode can create and run
containers without sudo.

If you want to also build with CUDA support, you must first make sure that your system has the
NVIDIA drivers installed. Additionally, running CUDA applications inside of a
Dev Container requires the installation of the `NVIDIA Container Toolkit
<https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html>`_.
Docker must be configured to use the NVIDIA Container toolkit, so make sure to
follow the `configuration guide
<https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html#configuring-docker>`_
as well.

You can then clone the project. Make sure to update the submodules as Sassena
relies upon the header-only library `taskflow <https://taskflow.github.io/>`_, which is distributed as a
submodule in the repo.

.. code-block:: bash

   git clone https://codebase.helmholtz.cloud/DAPHNE4NFDI/sassena.git
   cd sassena
   git submodule update --init --recursive

Once you have the required dependencies, you can open the folder in VSCode and
the editor will then ask whether you want to open the project in a container.
You can select either the CPU or the CPU+CUDA container depending on your needs.
If you don't get any prompt, you can follow the additional instructions in the
`Dev Container Documentation
<https://code.visualstudio.com/docs/devcontainers/attach-container>`_.

Once VSCode has built the container, it should ask you to configure the project,
which corresponds to the CMake configure step. If you only intend to run Sassena
and not develop further, pick either "Release (CPU)" or "Release (CPU+CUDA)".
You can then build the project using build button as normal or F7. Once it has
been built, you can open a terminal in VSCode (Terminal > New Terminal) and a
shell will appear pointing to the current build directory, where the sassena
binaries should be visible.

Other Editors
"""""""""""""

For optimal performance (e.g. using target-specific registers, CUDA), Sassena
can be compiled on the target machine, e.g. on Ubuntu 24.04 without using any
containers.

.. note::

   If you want to use the CUDA backend, you will need the NVIDIA drivers (not
   the open-source `nouveau`) as well as the `CUDA Toolkit
   <https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html>`_ as
   it contains not only the runtime libraries, but also libraries such as
   `cuFFT`, which the Sassena CUDA backend relies upon.

The process is in essence the same as building with VSCode, though it requires
manually configuring with CMake and compiling:

.. code-block:: bash

   sudo apt install build-essential cmake ninja-build git\
    openmpi-bin libblas-dev liblapack-dev \
    libxml2-dev libhdf5-dev zlib1g-dev libfftw3-dev\
    libboost-regex-dev libboost-mpi-dev\
    libboost-thread-dev libboost-serialization-dev libboost-system-dev\
    libboost-filesystem-dev libboost-program-options-dev
   git clone https://codebase.helmholtz.cloud/DAPHNE4NFDI/sassena.git
   cd sassena
   git submodule update --init --recursive
   cmake --preset=rel-cpu --build # or rel-cuda if you want to build with CUDA

Clusters
""""""""

If you are compiling Sassena on a cluster and are not able to install packages
such as Boost, you can still compile them locally and include them in the
Sassena build.

During the CMake configuration process, Sassena will look for the following
libraries:

* LibXml2
* ZLIB
* HDF5
* LAPACK
* spdlog
* FFTW3
* CudaToolkit (if using CUDA)
* Boost
* MPI

You can compile these separately and pass their locations to CMake. For example,
for Boost, this means setting the ``BOOST_ROOT`` location:

.. code-block:: bash

   cmake --preset=dev -DBOOST_ROOT=/opt/boost .

Please note that there is no standardised naming convention for these CMake
options. For each library, you must consult the relevant ``FindX`` page in the
CMake documentation, where ``X`` is the name of the library. For example, for
``HDF5``, the relevant CMake module is called `FindHDF5
<https://cmake.org/cmake/help/latest/module/FindHDF5.html>`_.

CMake Presets
"""""""""""""

Sassena defines several `CMake presets
<https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html>`_ which saves
time over configuring the CMake build manually as it sets default values for the
most common options. All of these values can be viewed or changed in
``CMakePresets.json``, though you can still override them using the command-line
as usual:

.. code-block:: bash

   cmake --preset=rel-cpu -DSASS_BUILD_DOCS=ON .

This would use the Release (CPU) preset but also build documentation, which is
usually disabled for a release build.

Depending on your use-case, you may wish to use a different preset, but we
recommend you stick with ``Release (CPU)`` for most cases:

.. list-table:: Sassena's CMake Presets
   :widths: 25 25 50
   :header-rows: 1

   * - Preset Display Name
     - Preset CMake Parameter
     - Explanation
   * - Dev (CPU)
     - ``--preset=dev``
     - This is the debug build for Sassena with CPU support only. It will have
       signficantly worse runtime performance than a release build but will
       allow you to debug the program with a debugger like ``gdb``. It also
       enables the developer mode, so tests will be built.
   * - Dev (CUDA)
     - ``--preset=dev-cuda``
     - This is also a debug build for Sassena except it also builds the project
       with CUDA support. This means you must have a CUDA-compatible GPU, the
       NVIDIA Drivers and the CUDA Toolkit installed.
   * - Release (CPU)
     - ``--preset=rel-cpu``
     - This is a release build of Sassena (i.e. optimisations are enabled) and
       supports CPU only.
   * - Release (CUDA)
     - ``--preset=rel-cuda``
     - This is a release build of Sassena (i.e. optimisations are enabled) and
       supports CUDA, so the same precautions must be taken as with ``dev-cuda``.


Building Unit Tests
"""""""""""""""""""

Sassena defines a CMake option ``USE_DEVELOPER_MODE``, which is automatically
enabled if you use either of the ``dev`` presets. If this option is enabled, then unit
tests are built.

The unit tests are run as a testing step on the CI but if you wish to run them
locally, they appear as a target in CMake. Therefore, depending on the
generator (e.g. Ninja, Make), you can execute

.. code-block:: bash

   ninja test

to run the tests. If you are using a release build and still want to run the
tests, you can pass the option ``-DUSE_DEVELOPER_MODE=ON`` to CMake in the
command-line as described in the CMake Presets section.

Building Documentation
""""""""""""""""""""""

If you want to build the documentation locally, then you must have Sphinx and
Doxygen installed, as well as ``doxysphinx`` and ``sphinx_rtd_theme`` (both of
which are written in Python and are available on ``pip``). All of these
dependencies are already included in the VSCode Dev Container.

You must then enable the ``SASS_BUILD_DOCS`` option in CMake. This can be done
by passing ``-DSASS_BUILD_DOCS=ON`` to CMake when configuring the project, for
example. If this succeeded, then the ``docs-sass`` target will be added to the
build, which can be built by running

.. code-block:: bash

   ninja docs-sass

if you are using the Ninja build generator (which is the default).

The ``.html`` files will be built in the ``docs/sphinx`` directory inside the
build folder.

Other Build Generators
""""""""""""""""""""""

CMake does not actually build the project itself but instead generates a build script
for a `build system`, e.g. Make, Visual Studio or XCode. Sassena uses Ninja by default as it is simple
and performant. However, if you don't want to use Ninja you can override the
default option, e.g.

.. code-block:: bash

   cmake --preset=rel-cpu -G "Unix Makefiles" .

will tell CMake to output Makefiles instead. You can then run ``make`` in the
build directory as usual.
