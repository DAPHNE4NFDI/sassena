**********
User Guide
**********


This section is designed to serve as a quick guide for users (who aren't
developers) to run Sassena.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :glob:

   installation
   running
   cmd-options
   config-options
