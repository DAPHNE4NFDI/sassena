debug
-----

::

	debug.timer = false // this adds a log message when a timer is started/stopped
	debug.barriers = false // this de-/activates collective barriers before each collective operation, this way all nodes are synchronized before the communication takes place. This is an important step towards analysis of timing.
	debug.monitor.update = true
	debug.iowrite.write = true
	debug.iowrite.buffer = true
	debug.print.orientations = false
