CI Pipeline
===========

Sassena has a CI piepline which runs on the GitLab instance when a new commit is
pushed. It compiles the project, runs unit and integration
tests and can launch deployments for the documentation and binaries.

Building the Docker Image
-------------------------

Sassena uses the same docker container to build the program and run the unit tests on
the CI server as well as for VSCode dev containers. Over time, packages may become need updating in the container, which can be done using the following steps.

The container which builds Sassena is defined in ``Dockerfile``, in the root of
the project directory ``/``. Additionally, there is a second container which supports CUDA and
which lives in ``.devcontainer/cuda/Dockerfile``. These files are
docker-specific shell scripts on how to build the container. They are both based on
Ubuntu but they download and install additional dependencies such as Boost and
Sphinx, which are needed for either compiling Sassena or during the CI pipeline.

Updating the docker containers is relatively simple. Firstly, you may need to
create a "Personal Access Token". This can be done in the Gitlab settings under
your personal profile settings. Once this is done, with your credentials, run
the command:

.. code-block::

    docker login registry.hzdr.de

Docker images are built on a local machine using the command:

.. code-block:: bash

  docker build -t registry.hzdr.de/daphne4nfdi/sassena .

This will build the `Dockerfile` in the current directory and store it with
the name ``registry.hzdr.de/daphne4nfdi/sassena``. If you have made changes to
the docker container and want to push it to the GitLab container registry and therefore CI, simply use the command:

.. code-block:: bash

  docker push registry.hzdr.de/daphne4nfdi/sassena


GitLab CI Configuration
-----------------------

The CI pipeline is defined by the file ``.gitlab-ci.yml``, which resides in the
root of the project. It is a YAML file and further information on its structure
can be found on the
`GitLab CI Documentation <https://docs.gitlab.com/ee/ci/>`_.

The file defines three main steps in the pipeline. Firstly, the project is
compiled at the pushed commit. This is done in exactly the same way as specified
in the README or User Guide section of the docs. Docs can also be built if the
user has Doxygen and Sphinx installed and passes the ``SASS_BUILD_DOCS=ON``
argument to CMake (see user guide for further information).

The test section of the pipeline runs unit and integration tests,
which could also be run locally if developer mode is enabled and the user runs
``ninja test`` (or similar if using ``make``) to run the ``test`` target. The
integration tests are simply ``scatter.xml`` files which live in the
``tests/waterdyn4/ci_configs`` directory and are compared to a known good
``signal.h5`` file on the CI using a Python script.

The final stage of the pipeline is the deployment step. For documentation, this
corresponds to copying the output of the docs from the build stage into a
specific directory for GitLab Pages. The deployment step also contains
a job which builds an AppImage of Sassena and uploads it to the "Releases"
section of the project using ``curl``.

AppImages are made using the utility `linuxdeploy
<https://github.com/meefik/linuxdeploy>`_, which is included in the Sassena
container. If you want to create your own AppImages locally, we recommend you
download and install linuxdeploy locally or use the existing one provided (e.g.
through VSCode's Dev Containers). AppImages are created according to the
`AppImage user
guide <https://docs.appimage.org/packaging-guide/from-source/native-binaries.html#cmake>`_
and then running linuxdeploy in the directory.
