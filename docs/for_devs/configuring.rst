Profiling with VTune
====================

This documentation is intended to help you configure ``Sassena`` and to
profile its performance by using profiling tool
``Intel VTune Profiler``.

Goals
-----

#. To help installing *Sassena*
#. To help installing Intel Basic oneAPI Toolkit
#. To help using VTune for profiling *Sassena*

Intel oneAPI Base Toolkit
-------------------------

*  The Intel® oneAPI Base Toolkit comprises a set of libraries and
   implementations for developing High-Performance applications in a
   range of different computer architectures.

*  VTune and Advisor were used for profiling the Sassena code with
   different setups of MPI cores and threads/core. Below, a quick
   instruction on the prerequisites and installation of the Intel®
   oneAPI Base Toolkit for *Linux* system applications are given. For a
   detailed and complete guide about the Intel® oneAPI Base Toolkit,
   please check out
   `here <https://www.intel.com/content/www/us/en/docs/oneapi/installation-guide-linux/2024-1/overview.html>`__.

Profiling with VTune GUI
------------------------

*  Within the Intel oneAPI Base Toolkit is VTune, which can be used for
   profiling HPC applications in C/C++ such as Sassena.

*  VTune can be set by a Graphical User Interface (GUI) on Linux OS:

#. Open the terminal and type: *vtune-gui*
#. Once the VTune GUI pops up, follow the steps below:

+----------------------+----------------------+------------------------+
| Step 01 - Start a    | Step 02 - Set paths  | Step 03 - Click the    |
| new project by       | for *Application*    | most right-bottom      |
| clicking on          | and *Application     | button and copy the    |
| *Configure Analysis* | Parameters*          | command to run VTune   |
|                      |                      | with MPI               |
+======================+======================+========================+
| |image9|             | |image10|            | |image11|              |
+----------------------+----------------------+------------------------+

#. Write the following parameters in **Step 02**:

   -  Application: global path of sassena binary application in compiler
      folder
   -  Application parameters: ``--config <global path of xml_file>``

#. Open the terminal and paste the command copied from **Step 03** in
   the following way:

   -  ``mpirun -np {Number of Cores} {paste here the command from step 03}``

* VTune with MPI only - Command Example:

   -  ``mpirun -np 8 /opt/intel/oneapi/vtune/2024.0/bin64/vtune -r /intel/vtune/projects/sassenaVtune/res cohe2-collect threading -target-duration-type=medium-data-limit=10000 -trace-mpi --app-working-dir=/home/newcode/intel/vtune/projects/sassenaVtune --/home/newcode/projects/helmholtz sassena/compile debug/sassena --config/home/newcode/projects/helmholtz/sassena/coherent/n str coh.xml``

*  VTune + MPI with Threads Limitation - Command Example:

   -  ``mpirun -np 8 /opt/intel/oneapi/vtune/2024.0/bin64/vtune -r /intel/vtune/projects/sassenaVtune/res cohe2 -collect threading -target-duration-type=medium -data-limit=10000 -trace-mpi --app-working-dir=/home/newcode/intel/vtune/projects/sassenaVtune --/home/newcode/projects/helmholtz/sassena/compile_debug/sassena --limits.computation.threads 2 --config/home/newcode/projects/helmholtz/sassena/coherent/n str coh.xml``

.. |image1| image:: img/virtual_box/virtual_box_01.png
.. |image2| image:: img/virtual_box/virtual_box_02.png
.. |image3| image:: img/virtual_box/virtual_box_03.png
.. |image4| image:: img/virtual_box/virtual_box_04.png
.. |image5| image:: img/virtual_box/virtual_box_05.png
.. |image6| image:: img/virtual_box/virtual_box_06.png
.. |image7| image:: img/virtual_box/virtual_box_07.png
.. |image8| image:: img/virtual_box/virtual_box_08.png
.. |image9| image:: img/vtune/vtune_new.png
.. |image10| image:: img/vtune/vtune1_new.png
.. |image11| image:: img/vtune/vtune1_new2.png
.. |image12| image:: img/CPU_info.PNG
.. |image13| image:: img/coherent/MPI/img/MPI_np8_cohe6.PNG
.. |image14| image:: img/coherent/MPI/img/MPI_np8_cohe5.PNG
.. |image15| image:: img/coherent/MPI/img/MPI_np8_cohe4.PNG
.. |image16| image:: img/coherent/MPI/img/MPI_np8_cohe3.PNG
.. |image17| image:: img/coherent/MPI/img/MPI_np8_cohe1.PNG
.. |image18| image:: img/coherent/Threading/img/Threading_2_cohe4.PNG
.. |image19| image:: img/coherent/Threading/img/Threading_2_cohe5.PNG
.. |image20| image:: img/coherent/Threading/img/Threading_2_cohe6.PNG
.. |image21| image:: img/coherent/Threading/img/Threading_2_cohe3.PNG
.. |image22| image:: img/coherent/Threading/img/Threading_2_cohe.PNG
.. |image23| image:: img/incoherent/MPI/img/MPI_np8_incoh6.PNG
.. |image24| image:: img/incoherent/MPI/img/MPI_np8_incoh5.PNG
.. |image25| image:: img/incoherent/MPI/img/MPI_np8_incoh4.PNG
.. |image26| image:: img/incoherent/MPI/img/MPI_np8_incoh3.PNG
.. |image27| image:: img/incoherent/MPI/img/MPI_np8_incoh1.PNG
.. |image28| image:: img/incoherent/Threading/img/Threading2_incohe3.PNG
.. |image29| image:: img/incoherent/Threading/img/Threading2_incohe2.PNG
.. |image30| image:: img/incoherent/Threading/img/Threading2_incohe5.PNG
.. |image31| image:: img/incoherent/Threading/img/Threading2_incohe4.PNG
.. |image32| image:: img/incoherent/Threading/img/Threading2_incohe.PNG
