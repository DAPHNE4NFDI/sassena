"""
Configuration file for the Sphinx documentation builder.

For the full list of built-in configuration values, see the documentation:
https://www.sphinx-doc.org/en/master/usage/configuration.html
"""

import os
import datetime

currentDateTime = datetime.datetime.now()
date = currentDateTime.date()

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Sassena'
version = os.environ.get('CMAKE_PROJECT_VERSION') or ''
author = 'Sassena Authors'
project_copyright = '{} {}'.format(date.year, author)

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx_rtd_theme']

root_doc = 'index'
html_static_path = ['_static']
html_logo = '_static/nav_logo.svg'

suppress_warnings = ['misc.highlighting_failure']

html_theme = 'sphinx_rtd_theme'

