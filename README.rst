=======
Sassena
=======

Sassena is a program to calculate X-ray and neutron scattering patterns from
Molecular Dynamics simulations in the Born approximation.

Description
===========

The full documentation is currently hosted on
https://daphne4nfdi.pages.hzdr.de/sassena (access restrictions apply).

Sassena can calculate:

* structure factor S(Q)
* coherent intermediate scattering function I\ :sup:`coh`\ (Q,t)
* incoherent intermediate scattering function I\ :sup:`inc`\ (Q,t)
* elastic incoherent structure factor EISF(Q)

Filetypes:

* Sassena can read ``pdb`` files for atom type definition and ``dcd`` files
  containg the trajectory. For trajectory conversion, use e.g. `VMD
  <https://www.ks.uiuc.edu/Research/vmd/>`_.
* The computation has to be defined by the user in an ``xml`` file
* Sassena writes the output into an ``hdf`` file

Orientational averages are calculated explicitly using a Monte Carlo scheme. It
is possible to define subsets of atoms or frames that the computation should run
on.

Sassena has a command-line interface and can be run across many CPUs using MPI.

Installation
============

You can choose to either use a pre-built binary of Sassena, which contains all
the required libraries or build the project yourself from source. For more
information, visit `the documentation <https://daphne4nfdi.pages.hzdr.de/sassena/for_users/index.html>`_.

AppImage
--------

An AppImage is provided that can run on Linux (x64, without CUDA support) out of the box when executed (no installation needed). To use
it, download the AppImage from the `releases page <https://codebase.helmholtz.cloud/DAPHNE4NFDI/sassena/-/releases>`_, mark it as an executable and run it from the
command-line:

.. code-block:: bash

   chmod +x Sassena.AppImage
   ./Sassena.AppImage # --config scatter.xml etc.
   # or to use mpi:
   mpirun -n 16 ./Sassena.AppImage --config scatter.xml

Compiling from Source
---------------------
For optimal performance (e.g. using target-specific registers, CUDA), Sassena
can be compiled on the target machine, e.g. on Ubuntu 24.04:

.. code-block:: bash

   sudo apt install build-essential cmake ninja-build git\
    openmpi-bin libblas-dev liblapack-dev\
    libxml2-dev libhdf5-dev zlib1g-dev libfftw3-dev\
    libboost-regex-dev libboost-mpi-dev\
    libboost-thread-dev libboost-serialization-dev libboost-system-dev\
    libboost-filesystem-dev libboost-program-options-dev
   git clone https://codebase.helmholtz.cloud/DAPHNE4NFDI/sassena.git
   cd sassena
   git submodule update --init --recursive
   cmake --preset=rel-cpu # or rel-cuda if you want to build with CUDA
   cd build-rel-cpu
   ninja

Contributing
============

To contribute, please create a feature branch from the develop branch and add
your changes and then create a merge request onto the `develop` branch. The repo
uses `rebasing workflow
<https://git-scm.com/book/en/v2/Git-Branching-Rebasing>`_, so you may need to
rebase your changes onto the `develop` branch before the merge can be completed.

For further information, please see ``CONTRIBUTING.rst``.


Notable upstream repositories
=============================

https://github.com/camm/sassena

https://github.com/benlabs/sassena

Citation
========

If you use Sassena for your calculations please cite

   Benjamin Lindner and Jeremy C. Smith: "Sassena — X-ray and neutron scattering calculated from molecular dynamics trajectories using massively parallel computers", Comp. Phys. Comm. 183/7 (2012) 1491, `DOI: 10.1016/j.cpc.2012.02.010 <http://dx.doi.org/10.1016/j.cpc.2012.02.010>`_
